function nErr = COM_LineFitError(dX, dY, nTargetX, nTargetY)
dA = COM_LineFit(dX, dY);
nA = dA(1);
nB = dA(2);

nEstTargetY = nA .* nTargetX + nB;
nTargetErr = abs(nEstTargetY - nTargetY);

nErr = nTargetErr;

% 
% figure(400);
% clf;
% hold on;
% plot(dX, dY, 'r');
% plot(dX, dEstY, 'b');
% plot(dX, dY, 'rx');
% plot(dX, dEstY, 'bx');
% hold off;
