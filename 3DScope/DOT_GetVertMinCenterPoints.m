function [bFound, dVertCenterPoint, dVertMeanLine] = DOT_GetVertMinCenterPoints(dRefData, dHorizMeanLine, nLocalMinThreshold, dFilter, nPosX)
[nSizeY nSizeX] = size(dRefData);

%% Get vertical line data and y position
nMinPosX = nPosX;
nMaxPosX = nPosX;
while (dHorizMeanLine(nMinPosX) < nLocalMinThreshold) && (nMinPosX > 1);
    nMinPosX = nMinPosX - 1; 
end;
while (dHorizMeanLine(nMaxPosX) < nLocalMinThreshold) && (nMaxPosX < nSizeX);
    nMaxPosX = nMaxPosX + 1; 
end;

dVertMeanLine = mean(dRefData(:, nMinPosX:nMaxPosX)');
dVertMeanLine=imfilter(dVertMeanLine, dFilter,'replicate','conv');

[bFound, dVertCenterPoint] = COM_FindLineLocalMinPoint(dVertMeanLine, nLocalMinThreshold);