clear all;
close all;
format shortg;

%% Data Path
%szDataPath = 'Z:\Data(2015-05-07)\측정안됨\AnalysisData\';
%szDataPath = 'Z:\Data(2015-03-10)\Red\AnalysisData\';
szDataPath = 'Z:\Data(2015-05-07)\3D_코너측정\20X\AnalysisData\';
%szDataPath = 'Z:\Data(2015-05-07)\3D_코너측정\50X\AnalysisData\';

%% DOT data
%szFileName = '50X_2_0-0_2015-05-07-09-57-32-880';
%szFileName = '01_1-0_2015-05-07-12-01-12-853';
szFileName = 'M_20150508101953DOT_01_1-0_2015-05-19-09-42-46-698';
%szFileName = 'M_20150508103400DOT_01_1-0_2015-05-19-09-43-18-253';

%%

dAnalysis = COM_ReadAnaysisData(szDataPath, szFileName);
dHeightData = COM_ReadHeightData(szDataPath, szFileName, false);

% Test for checking cornder distance
%dHeightData = fliplr(dHeightData);
%dHeightData = flipud(dHeightData);

% for test
dParameter.nmPerPixel = dAnalysis.nmPerPixel;
dParameter.nmPerHeight = dAnalysis.nmPerHeight;
dParameter.nTargetThick = dAnalysis.nTargetThick/1000;
dParameter.nGlassSpaceGap = dAnalysis.nGlassSpaceGap;

if (dAnalysis.nMeasureAngle == 1); szAngle = 'd';
else; szAngle = 'n'; end;

if (dAnalysis.bMeasureAll == 1); bMeasureAll = true;
else; bMeasureAll = false; end;

dParameter.nGlassSpaceGap = 0;
%szAngle = 'd';
%bMeasureAll = false;

dIndexCheck = [];
[bSuccess dRun] = DOT_Measure(dHeightData, bMeasureAll, szAngle, dParameter, dIndexCheck);
%COM_ShowMeasureResult(dRun.dMeasure, bMeasureAll, szAngle, dParameter);
