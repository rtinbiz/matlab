function [dProfile] = DOT_GetHeightProfile(dData, nStartPosX, nStartPosY, nEndPosX, nEndPosY, bShowGraph)
[nSizeY nSizeX] = size(dData);
nHeight = max(dData(:));

nLengthX = abs(nEndPosX - nStartPosX) + 1;
nLengthY = abs(nEndPosY - nStartPosY) + 1;

if(nLengthX == 1);
    nLength = nLengthY;
elseif(nLengthY == 1);
    nLength = nLengthX;
else;
    nLength = min([nLengthX nLengthY]);
end;

dProfile = zeros(1, nLength);

for i = 0:nLength - 1;
    nDeltaX = sign(nEndPosX - nStartPosX) * i;
    nDeltaY = sign(nEndPosY - nStartPosY) * i;
    dProfile(i+1) = dData(nStartPosY + nDeltaY, nStartPosX + nDeltaX);
end;

if bShowGraph; 
    figure(121);
    clf;
    set(gcf,'position',[300 300 (300+800) (300+100)]);
    
    subplot(1,2,1);
    hold on;
    mesh(dData);
    axis([0 nSizeX 0 nSizeY 0 nHeight]);
    view([0 -90]); 

    plot(nStartPosX, nStartPosY, 'rx');
    szText = sprintf('Start Point (%d, %d)', nStartPosX, nStartPosY); 
    text(nStartPosX, nStartPosY + 50, szText, 'Horizontalalignment','center');
    
    plot(nEndPosX, nEndPosY, 'bx');
    szText = sprintf('End Point (%d, %d)', nEndPosX, nEndPosY); 
    text(nEndPosX, nEndPosY - 50, szText, 'Horizontalalignment','center');

    szText = sprintf('Length (%d)', nLength); 
    text((nEndPosX+nStartPosX)/2, (nEndPosY+nStartPosY)/2, szText, 'Horizontalalignment','center');
    
    plot([nStartPosX nEndPosX], [nStartPosY nEndPosY], 'k');
    hold off;
    
    subplot(1,2,2);
    plot(dProfile);
    nHeightMargin = (max(dProfile) - min(dProfile)) * 0.1;
    axis([0 nLength (min(dProfile)-nHeightMargin) (max(dProfile)+nHeightMargin)]);
    drawnow;
end;
