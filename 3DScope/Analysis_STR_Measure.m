clear all;
close all;
format shortg;

%% Data Path
%szDataPath = '..\..\Data\AnalysisData\';
szDataPath = 'Z:\Measer3D\AnalysisData\';
%% DOT data
szFileName = 'STRIPE_31_2014_11_24_21_51_44';

disp('=====================');
disp('Start Analysis Data!!');
disp(strcat(szDataPath,szFileName));
disp('=====================');

dAnalysis = COM_ReadAnaysisData(szDataPath, szFileName);
dHeightData = COM_ReadHeightData(szDataPath, szFileName, false);

dParameter.nmPerPixel = dAnalysis.nmPerPixel;
dParameter.nmPerHeight = dAnalysis.nmPerHeight;
dParameter.nTargetThick = dAnalysis.nTargetThick/1000;
dParameter.nGlassSpaceGap = dAnalysis.nGlassSpaceGap;

if (dAnalysis.nMeasureAngle == 1); szAngle = 'd';
else; szAngle = 'n'; end;

if (dAnalysis.bMeasureAll == 1); bMeasureAll = true;
else; bMeasureAll = false; end;

[bSuccess dRun] = STR_Measure(dHeightData, dParameter);
if(bSuccess == false);    
    disp('[Failure !!] Error occurred in measuring data.');
end;

MASK_TYPE_STRIPE = 1;
MASK_TYPE_DOT = 2;

nIndex=0; bIdentical = [];
nIndex=nIndex+1; bIdentical(nIndex) = COM_CompareData('RefImage', dAnalysis.dRefImage, dRun.dRefImage, false);
nIndex=nIndex+1; bIdentical(nIndex) = COM_CompareData('HorizMeanLine', dAnalysis.dHorizMeanLine, dRun.dHorizMeanLine, false);
nIndex=nIndex+1; bIdentical(nIndex) = COM_CompareData('HorizMinCenterPoints', dAnalysis.dHorizMinCenterPoints, dRun.dHorizMinCenterPoints, true);

if(length(dAnalysis.dMeasure) ~= length(dRun.dMeasure));
    nIndex=nIndex+1; bIdentical(nIndex) = 0;
    strText = sprintf('* Compare : Measure Data -> Different length !!, Analysis Data(%d), Run Data(%d)', length(dAnalysis.dMeasure), length(dRun.dMeasure));
    disp(strText);
else;
    for(i=1:length(dAnalysis.dMeasure));
        nIndexCheck = -1;
        if(nIndexCheck > 0 && i ~= nIndexCheck);
            continue;
        end;
        
        strText = sprintf('[Compare : Measure Data (%d/%d)]', i, length(dAnalysis.dMeasure));
        disp(strText);
        
        nIndexStart = nIndex+1;
        nIndex=nIndex+1; bIdentical(nIndex) = COM_CompareData('StartPoint', dAnalysis.dMeasure(i).dStartPoint, dRun.dMeasure(i).dStartPoint, true);
        nIndex=nIndex+1; bIdentical(nIndex) = COM_CompareData('EndPoint', dAnalysis.dMeasure(i).dEndPoint, dRun.dMeasure(i).dEndPoint, true);
        nIndex=nIndex+1; bIdentical(nIndex) = COM_CompareData('HeightProfile', dAnalysis.dMeasure(i).dHeightProfile, dRun.dMeasure(i).dHeightProfile, false);
        nIndex=nIndex+1; bIdentical(nIndex) = COM_CompareData('AngleProfile', dAnalysis.dMeasure(i).dAngleProfile, dRun.dMeasure(i).dAngleProfile, false);
        nIndex=nIndex+1; bIdentical(nIndex) = COM_CompareData('AngleMinPoints', dAnalysis.dMeasure(i).dAngleMinPoints, dRun.dMeasure(i).dAngleMinPoints, true);
        nIndex=nIndex+1; bIdentical(nIndex) = COM_CompareData('FeaturePointLeft', dAnalysis.dMeasure(i).dFeaturePointLeft, dRun.dMeasure(i).dFeaturePointLeft, true);
        nIndex=nIndex+1; bIdentical(nIndex) = COM_CompareData('FeaturePointRight', dAnalysis.dMeasure(i).dFeaturePointRight, dRun.dMeasure(i).dFeaturePointRight, true);
        nIndex=nIndex+1; bIdentical(nIndex) = COM_CompareResultData('ResultData', dAnalysis.dMeasure(i).dResult, dRun.dMeasure(i).dResult);
        nIndexEnd = nIndex;
        if(sum(bIdentical(nIndexStart:nIndexEnd)) == (nIndexEnd - nIndexStart + 1)); 
            dAnalysis.dResultDifferent(i) = false;
        else;
            dAnalysis.dResultDifferent(i) = true;
        end;
    end;
end;

disp('=====================');
if sum(bIdentical) == nIndex;
    disp('End Analysis Data : Success !! All data are identical.');
else;
    disp('[Different Measure Data Index]');
    dIndex = find(dAnalysis.dResultDifferent == 1)
    disp('End Analysis Data : Failure !! Different data exists.');
end;
disp('=====================');

% i = 
% dAnalysis.dMeasure(i)
% dRun.dMeasure(i)
% COM_CompareResultData('ResultData', dAnalysis.dMeasure(i).dResult, dRun.dMeasure(i).dResult);


