function [bSuccess, dRun] = STR_Measure(dHeightData, dParameter)
bSuccess = false;
dMeasureData = [];

bShowGraph_FindStartCenterPoint = false;
bShowGraph_FindEndCenterPoint = false;
bShowGraph_GetHeightProfile = true;
bShowGraph_MeasureHeightProfilet = true;

%% Get Hieght Reference Data
dRefData = STR_GetRefHeightData(dHeightData);
dRun.dRefImage = dRefData(1,:);

%% Find Start Center Point
[bSuccess nStartPosX nStartPosY dHorizMeanLine dHorizMinPoints] = STR_FindStartCenterPoint(dHeightData, dRefData, bShowGraph_FindStartCenterPoint);
if(bSuccess == false);
    return;
end;

dRun.dHorizMeanLine = dHorizMeanLine;
dRun.dHorizMinCenterPoints = dHorizMinPoints;

%% Find End Center Point
[bSuccess nEndPosX nEndPosY] = STR_FindEndCenterPoint(dHeightData, dRefData, dHorizMeanLine, dHorizMinPoints, nStartPosX, nStartPosY, bShowGraph_FindEndCenterPoint);
if(bSuccess == false);
    return;
end;

i = 1;

dRun.dMeasure(i).dStartPoint = [nStartPosX nStartPosY];
dRun.dMeasure(i).dEndPoint = [nEndPosX nEndPosY];

dHeightProfile = STR_GetHeightProfile(dHeightData, nStartPosX, nStartPosY, nEndPosX, nEndPosY, bShowGraph_GetHeightProfile);
dRun.dMeasure(i).dHeightProfile = dHeightProfile;
    
[dResult dAngleProfie dAngleMinPoint dFeaturePoint_L dFeaturePoint_R] = STR_MeasureHeightProfile(dHeightProfile, 'n', dParameter, bShowGraph_MeasureHeightProfilet);
dRun.dMeasure(i).dAngleProfile = dAngleProfie;
dRun.dMeasure(i).dAngleMinPoints = dAngleMinPoint;
dRun.dMeasure(i).dFeaturePointLeft = [dFeaturePoint_L.nHighPos dFeaturePoint_L.nMidPos dFeaturePoint_L.nLowPos];
dRun.dMeasure(i).dFeaturePointRight = [dFeaturePoint_R.nHighPos dFeaturePoint_R.nMidPos dFeaturePoint_R.nLowPos];
dRun.dMeasure(i).dResult = dResult;

