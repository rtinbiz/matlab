function [dData] = COM_ReadHeightData(szDataPath, szFileName, bShowGraph)
szFilePath=sprintf('%s%s_H.csv',szDataPath,szFileName);
dData = csvread(szFilePath);

if bShowGraph;
    [nSizeY nSizeX] = size(dData);
    nHeight = max(dData(:));

    figure;
    imshow(dData./nHeight);

    hold on;
    figure;
    subplot(1,2,1);
    mesh(dData);
    axis([0 nSizeX 0 nSizeY 0 nHeight]);
    view([0 90]); 

    subplot(1,2,2);
    mesh(dData);
    axis([0 nSizeX 0 nSizeY 0 nHeight]);
    view([-30 70]); 
    hold off;
end;
