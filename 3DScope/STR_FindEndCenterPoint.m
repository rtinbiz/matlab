function [bFound nPosX nPosY] = STR_FindEndCenterPoint(dData, dRefData, dHorizMeanLine, dCenterPointX, nStartPosX, nStartPosY, bShowGraph)
[nSizeY nSizeX] = size(dData);
nHeight = max(dData(:));
nRefDataHeight = max(dRefData(:));

%% Get vertical line data and x position
nStartIndexX = find(dCenterPointX == nStartPosX);

bFound = false;

% right position
nPosY = nStartPosY;
nIndexX = nStartIndexX + 1;
while(nIndexX <= length(dCenterPointX));
    nPosX = dCenterPointX(nIndexX);
    if(dRefData(nPosX) < 1);
        bFound = true;
        break;
    end;
    nIndexX = nIndexX + 1;
end;

% left position
if(bFound == false);
    nIndexX = nStartIndexX - 1;
    while(nIndexX >= 1);
        nPosX = dCenterPointX(nIndexX);
        if(dRefData(nPosX) < 1);
            bFound = true;
            break;
        end;
        nIndexX = nIndexX - 1;
    end;
end;

%% Show Graph
if bShowGraph; 
    figure(111);
    clf;
    subplot(2,1,1);
    mesh(dData);
    axis([0 nSizeX 0 nSizeY 0 nHeight]);
    view([0 -90]);
    hold on;
    plot(nPosX, nPosY, 'rx');
    text(nPosX, nPosY + 70, 100, 'End Point', 'Horizontalalignment','center');
    hold off;
    
    subplot(2,1,2);
    hold on;
    plot(dHorizMeanLine);
    plot(dCenterPointX, dHorizMeanLine(dCenterPointX), 'rx')    
    axis([0 nSizeX 0 nRefDataHeight]);
    hold off;
    
    drawnow;
end;

