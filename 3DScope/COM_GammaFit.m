function [dA nMeanErr] = COM_GammaFit(dX, dY);
dLogX = log(dX);
dLogY = log(dY);

dALineFit = COM_LineFit(dLogX, dLogY);
nA = exp(dALineFit(2));
nB = dALineFit(1);
dA = [nA nB];

dEstY = nA .* (dX .^ nB);
nMeanErr = mean(abs(dEstY - dY));    

