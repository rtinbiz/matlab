function [bFound, nIndex] = COM_CheckMeanLineMargin(dLine, nMarginSize, bReverse);
if(bReverse);
    dLine = fliplr(dLine);
end;

nLength = length(dLine);

bFound = false;
nIndex = 0;
for i=1:nLength;
    if(dLine(i) < 1);
        if(i >= nMarginSize);
            bFound = true;
            nIndex = i - 1;
            if(bReverse);
                nIndex = nLength - nIndex + 1;
            end;
        end;
        
        return;
    end;
end;
