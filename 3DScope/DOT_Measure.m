function [bSuccess, dRun] = DOT_Measure(dHeightData, bMeasureAll, szAngle, dParameter, dIndexCheck)
bSuccess = false;

bShowGraph_SmoothingHeightData = false;
bShowGraph_FindCenterPoints = true;
bShowGraph_CheckCornerDistance = true;
bShowGraph_GetHeightProfile = false;
bShowGraph_MeasureHeightProfilet = false;

%% Smoothing Hieght Data
dHeightData = COM_SmoothingHeightData(dHeightData, bShowGraph_SmoothingHeightData);

%% Get Hieght Reference Data
[dRefData, dRefMeanLineData] = DOT_GetRefHeightData(dHeightData);
dRun.dRefImage = dRefData(1,:);

%% Find Start Center Points
[bSuccess dHorizMinPoints dVertMinPoints dHorizMeanLine dVertMeanLine] = DOT_GetMinCenterPoints(dHeightData, dRefData, bShowGraph_FindCenterPoints);
if(bSuccess == false);
    return;
end;

dRun.dHorizMeanLine = dHorizMeanLine;
dRun.dHorizMinCenterPoints = dHorizMinPoints;
dRun.dVertMeanLine = dVertMeanLine;
dRun.dVertMinCenterPoints = dVertMinPoints;

nCornerMarginSize = 3;
[nCornerDistance, dPos] = DOT_CheckCornerDistance(dHeightData, dHorizMeanLine, dHorizMinPoints, dVertMeanLine, dVertMinPoints, nCornerMarginSize, dParameter, bShowGraph_CheckCornerDistance);
dRun.dCornerDistance = nCornerDistance;
if(nCornerDistance > 0);
    nCornerDistance
    dPos
end;

%% Find Start Center Point
[bSuccess dMeasurePoints] = DOT_GetMeasurePoints(dHeightData, dRefData, dHorizMinPoints, dVertMinPoints, bMeasureAll, szAngle, bShowGraph_FindCenterPoints);
if(bSuccess == false);
    return;
end;

for(i=1:length(dMeasurePoints));
    if(~isempty(dIndexCheck) && isempty(find(dIndexCheck==i)));
        continue;
    end;

    strText = sprintf('Measuring : %d/%d', i, length(dMeasurePoints));
    disp(strText);
    
    nStartPosX = dMeasurePoints(i).nStartPosX;
    nStartPosY = dMeasurePoints(i).nStartPosY;
    nEndPosX = dMeasurePoints(i).nEndPosX;
    nEndPosY = dMeasurePoints(i).nEndPosY;

    dRun.dMeasure(i).dStartPoint = [nStartPosX nStartPosY];
    dRun.dMeasure(i).dEndPoint = [nEndPosX nEndPosY];
    
    dHeightProfile = DOT_GetHeightProfile(dHeightData, nStartPosX, nStartPosY, nEndPosX, nEndPosY, bShowGraph_GetHeightProfile);
    dRun.dMeasure(i).dHeightProfile = dHeightProfile;

    [dResult dInverseHeightProfile dAngleProfie dAngleMinPoint dFeaturePoint_L dFeaturePoint_R] = COM_MeasureHeightProfile(dHeightProfile, szAngle, dParameter, bShowGraph_MeasureHeightProfilet);
    dRun.dMeasure(i).dInverseHeightProfile = dInverseHeightProfile;
    dRun.dMeasure(i).dAngleProfile = dAngleProfie;
    dRun.dMeasure(i).dAngleMinPoints = dAngleMinPoint;
    dRun.dMeasure(i).dFeaturePointLeft = [dFeaturePoint_L.nHighPos dFeaturePoint_L.nMidPos dFeaturePoint_L.nLowPos];
    dRun.dMeasure(i).dFeaturePointRight = [dFeaturePoint_R.nHighPos dFeaturePoint_R.nMidPos dFeaturePoint_R.nLowPos];
    dRun.dMeasure(i).dResult = dResult;
end;
   
