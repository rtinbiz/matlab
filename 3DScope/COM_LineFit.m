function [dA nMeanErr] = COM_LineFit(dX, dY)
dX2 = [dX; ones(1, length(dX))];
dA = dY * pinv(dX2);
nA = dA(1);
nB = dA(2);

dEstY = nA .* dX + nB;
nMeanErr = mean(abs(dEstY - dY));

% 
% figure(400);
% clf;
% hold on;
% plot(dX, dY, 'r');
% plot(dX, dEstY, 'b');
% plot(dX, dY, 'rx');
% plot(dX, dEstY, 'bx');
% hold off;
