function [nDistance, dPos] = DOT_CheckCornderDistance(dHeightData, dHorizMeanLine, dHorizMinPoints, dVertMeanLine, dVertMinPoints, nCornerMarginSize, dParameter, bShowGraph)
nLengthX = length(dHorizMeanLine);
nLengthY = length(dVertMeanLine);

nCenterX = nLengthX/2;
nCenterY = nLengthY/2;

bCornerLeft = false;
bCornerRight = false;
bCornerTop = false;
bCornerBottom = false;

% Check Left
[bCornerLeft, nIndexLeft] = COM_CheckMeanLineMargin(dHorizMeanLine, nCornerMarginSize, false);
[bCornerRight, nIndexRight] = COM_CheckMeanLineMargin(dHorizMeanLine, nCornerMarginSize, true);
[bCornerTop, nIndexTop] = COM_CheckMeanLineMargin(dVertMeanLine, nCornerMarginSize, false);
[bCornerBottom, nIndexBottom] = COM_CheckMeanLineMargin(dVertMeanLine, nCornerMarginSize, true);

nCornerCount = 0;
if(bCornerLeft); nCornerCount = nCornerCount + 1; end;
if(bCornerRight); nCornerCount = nCornerCount + 1; end;
if(bCornerTop); nCornerCount = nCornerCount + 1; end;
if(bCornerBottom); nCornerCount = nCornerCount + 1; end;

if(nCornerCount ~= 2);
    nDistance = 0;
    dPos = [];
    return;
end;

nStartX = 1;
nEndX = nLengthX;
nStartY = 1;
nEndY = nLengthY;
nGapX = 0;
nGapY = 0;

nGapX = dHorizMinPoints(2) - dHorizMinPoints(1);
nGapY = dVertMinPoints(2) - dVertMinPoints(1);

if(bCornerLeft);
    nStartX = 4
    nEndX = dHorizMinPoints(1);
    if(bCornerTop);
        nStartY = dVertMinPoints(1) - floor(nGapY/4);
        nEndY = dVertMinPoints(1) + floor(nGapY/4);;
    else;
        nStartY = dVertMinPoints(end) - floor(nGapY/4);
        nEndY = dVertMinPoints(end) + floor(nGapY/4);
    end;        

    dSubImage = dHeightData(nStartY:nEndY, nStartX:nEndX);
    dHorizMeanLine = mean(dSubImage);

    nIndexLeft = DOT_GetCornerPos(dHorizMeanLine, dParameter, false);
end;
if(bCornerRight);
    nStartX = dHorizMinPoints(end);
    nEndX = nLengthX;
    if(bCornerTop);
        nStartY = dVertMinPoints(1) - floor(nGapY/4);;
        nEndY = dVertMinPoints(1) + floor(nGapY/4);;
    else;
        nStartY = dVertMinPoints(end) - floor(nGapY/4);;
        nEndY = dVertMinPoints(end) + floor(nGapY/4);;
    end;        
    
    dSubImage = dHeightData(nStartY:nEndY, nStartX:nEndX);
    dHorizMeanLine = mean(dSubImage);
    
    nIndexRight = DOT_GetCornerPos(dHorizMeanLine, dParameter, true);
    nIndexRight = nIndexRight + nStartX - 1;
end;        

if(bCornerTop);
    nStartY = 1;
    nEndY = dVertMinPoints(1);
    if(bCornerLeft);
        nStartX = dHorizMinPoints(1) - floor(nGapX/4);
        nEndX = dHorizMinPoints(1) + floor(nGapX/4);
    else;
        nStartX = dHorizMinPoints(end) - floor(nGapX/4);
        nEndX = dHorizMinPoints(end) + floor(nGapX/4);
    end;        

    dSubImage = dHeightData(nStartY:nEndY, nStartX:nEndX);
    dVertMeanLine = mean(dSubImage');
    
    nIndexTop = DOT_GetCornerPos(dVertMeanLine, dParameter, false);
end;
if(bCornerBottom);
    nStartY = dVertMinPoints(end);
    nEndY = nLengthY;
    if(bCornerLeft);
        nStartX = dHorizMinPoints(1) - floor(nGapX/4);
        nEndX = dHorizMinPoints(1) + floor(nGapX/4);
    else;
        nStartX = dHorizMinPoints(end) - floor(nGapX/4);
        nEndX = dHorizMinPoints(end) + floor(nGapX/4);
    end;        

    dSubImage = dHeightData(nStartY:nEndY, nStartX:nEndX);
    dVertMeanLine = mean(dSubImage');
    
    nIndexBottom = DOT_GetCornerPos(dVertMeanLine, dParameter, true);
    nIndexBottom = nIndexBottom + nStartY - 1;
end;        

nPosX = 0;
nPosY = 0;
if(bCornerLeft);
    nPosX = nIndexLeft - nCenterX;
end;
if(bCornerRight);
     nPosX = nIndexRight - nCenterX;
end;
if(bCornerTop);
    nPosY = nIndexTop - nCenterY;
end;
if(bCornerBottom);
    nPosY = nIndexBottom - nCenterY;
end;        

nDistance = norm([nPosX nPosY]);
dPos = [nCenterX+nPosX nCenterY+nPosY];

%% Show Graph
if bShowGraph; 
    figure(201);
    clf;

    nPosX = dPos(1);
    nPosY = dPos(2);
    
    hold on;
    imshow(uint8(dHeightData/max(dHeightData(:))*255));
    line([nCenterX nPosX], [nCenterY nPosY], 'Color', 'red');
    
    if(bCornerLeft);
        nStartX = 1;
        nEndX = nPosX + nGapX;
    end;
    if(bCornerRight);
        nStartX = nPosX - nGapX;
        nEndX = nLengthX;
    end;    
    if(bCornerTop);
        nStartY = 1;
        nEndY = nPosY + nGapY;
    end;
    if(bCornerBottom);
        nStartY = nPosY - nGapY;
        nEndY = nLengthY;
    end;
    
    line([nStartX nEndX], [nPosY nPosY], 'Color', 'blue');
    line([nPosX nPosX], [nStartY nEndY], 'Color', 'blue');
    
    hold off;
end;
