function [dResult dAngleProfile dAnglePoint dFeaturePoint_L dFeaturePoint_R] = STR_MeasureHeightProfile(dHeightProfile, szDir, dParameter, bShowGraph)
[dResult dAngleProfile dAnglePoint dFeaturePoint_L dFeaturePoint_R] = COM_MeasureHeightProfile(dHeightProfile, szDir, dParameter, bShowGraph);
dResult.HEIGHT = dParameter.nTargetThick;
dResult.HEIGHT_TOTAL_L = dResult.HEIGHT;
dResult.HEIGHT_TOTAL_R = dResult.HEIGHT;
dResult.HEIGHT_E_L = dResult.HEIGHT - dResult.HEIGHT_L;
dResult.HEIGHT_E_R = dResult.HEIGHT - dResult.HEIGHT_R;
