function [dMinPoint] = COM_FindAngleLocalMinPoint(dHeightProfile, dAngleProfile, nThreshold)
nLength = length(dAngleProfile);

dPoint = [];

nBarSize = 1;
bDirDownCur = true;
bDirUpNext = false;
for i=nBarSize+1:nLength-nBarSize;
    nDiffCur = dAngleProfile(i) - mean(dAngleProfile(i-nBarSize : i - 1));
    nDiffNext = mean(dAngleProfile(i+1 : i + nBarSize)) - dAngleProfile(i);
    
    if(nDiffCur < 0) bDirDownCur = true;
    elseif (nDiffCur > 0) bDirDownCur = false;
    % if nDiffCur == 0, then don't change bDirDownCur.
    end;
       
    if(nDiffNext > 0) bDirUpNext = true;
    else bDirUpNext = false;
    end;
    
    if(dAngleProfile(i)<nThreshold);
        if (bDirDownCur && bDirUpNext);
            dPoint = [dPoint i];
        end;
    end;
 end;

dMinPoint = dPoint;

% add min height point of both side
% nHalfPos = floor(nLength/2);
% [nMin nPos] = min(dHeightProfile(1:nHalfPos-1));
% if(isempty(find(dMinPoint == nPos)));
%     dMinPoint = [dMinPoint nPos];
% end;
% 
% [nMin nPos] = min(dHeightProfile(nHalfPos:end));
% nPos = nHalfPos + nPos - 1;
% if(isempty(find(dMinPoint == nPos)));
%     dMinPoint = [dMinPoint nPos];
% end;
% 
% dMinPoint = sort(dMinPoint);

