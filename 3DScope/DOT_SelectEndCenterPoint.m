function [bFound, nStartPosX, nStartPosY, nEndPosX, nEndPosY] = DOT_SelectEndCenterPoint(dRefData, dHorizMinPoints, dVertMinPoints, nStartPosX, nStartPosY, bMeasureAll, szAngle, szDir)
[nSizeY nSizeX] = size(dRefData);

bDiagonal = false;
bNormal= false;
bHoriz = false;
bVert = false;

szAngle=lower(szAngle);
if ~isempty(strfind(szAngle,'d'));        % diagonal (45 degree)
    bDiagonal = true;
elseif ~isempty(strfind(szAngle,'n'));    % Normal (0 degree)
    bNormal = true;
end;

szDir=lower(szDir);
if ~isempty(strfind(szDir,'h'));        % horiz (0 degree)
    bHoriz = true;
elseif ~isempty(strfind(szDir,'v'));    % vert (90 degree)
    bVert = true;
end;

%% Get vertical line data and x position
nStartIndexX = find(dHorizMinPoints == nStartPosX);
nStartIndexY = find(dVertMinPoints == nStartPosY);

bFound = false;
nEndPosX = 0;
nEndPosY = 0;
% When MesureAll is true, check only right and down.
% otherwise, check right/left and down/up.
if(bNormal);
    if(bHoriz);
        % right position
        nEndPosY = nStartPosY;
        nIndexX = nStartIndexX + 1;
        while(nIndexX <= length(dHorizMinPoints));
            nEndPosX = dHorizMinPoints(nIndexX);
            if(dRefData(nEndPosY, nEndPosX) < 1);
                bFound = true;
                break;
            end;
            nIndexX = nIndexX + 1;
        end;

        if(bFound == false && bMeasureAll == false);
            % left position
            nEndPosY = nStartPosY;
            nIndexX = nStartIndexX - 1;
            while(nIndexX >= 1);
                nEndPosX = dHorizMinPoints(nIndexX);
                if(dRefData(nEndPosY, nEndPosX) < 1);
                    bFound = true;
                    break;
                end;
                nIndexX = nIndexX - 1;
            end;
        end;        
    end;
    
    if(bVert);
        % down position
        nEndPosX = nStartPosX;
        nIndexY = nStartIndexY + 1;
        while(nIndexY <= length(dVertMinPoints));
            nEndPosY = dVertMinPoints(nIndexY);
            if(dRefData(nEndPosY, nEndPosX) < 1);
                bFound = true;
                break;
            end;

            nIndexY = nIndexY + 1;
        end;
        
        if(bFound == false && bMeasureAll == false);
        % up position
            nEndPosX = nStartPosX;
            nIndexY = nStartIndexY - 1;
            while(nIndexY >= 1);
                nEndPosY = dVertMinPoints(nIndexY);
                if(dRefData(nEndPosY, nEndPosX) < 1);
                    bFound = true;
                    break;
                end;
                nIndexY = nIndexY - 1;
            end;
        end;
    end;
end;

% When MesureAll is true, check only left-up and right-up.
% otherwise, check left-up/right-down and right-up/left-down. 
if(bDiagonal);
    nStartPosOrigX = nStartPosX;
    nStartPosOrigY = nStartPosY;
    
    if(bHoriz);
        % right-down position
        nIndexX = nStartIndexX + 1;
        nIndexY = nStartIndexY + 1;
        while(nIndexX <= length(dHorizMinPoints) && nIndexY <= length(dVertMinPoints));
            nStartPosX = nStartPosOrigX;
            nStartPosY = nStartPosOrigY;
            nEndPosX = dHorizMinPoints(nIndexX);
            nEndPosY = dVertMinPoints(nIndexY);
            
            nLengthX = abs(nEndPosX - nStartPosX);
            nLengthY = abs(nEndPosY - nStartPosY);
            nLength = max([nLengthX nLengthY]);
            if(nStartPosX + nLength <= nSizeX);
                nEndPosX = nStartPosX + nLength;
            else;
                nStartPosX = nEndPosX - nLength;
            end;
            if(nEndPosY + nLength <= nSizeY);
                nEndPosY = nStartPosY + nLength;
            else;
                nStartPosY = nEndPosY - nLength;
            end;
            
            if(dRefData(nStartPosY, nStartPosX) < 1 && dRefData(nEndPosY, nEndPosX) < 1);
                bFound = true;
                break;
            end;
            nIndexX = nIndexX + 1;
            nIndexY = nIndexY + 1;
        end;
        
        if(bFound == false && bMeasureAll == false);
            % left-up position
            nIndexX = nStartIndexX - 1;
            nIndexY = nStartIndexY - 1;
            while(nIndexX >= 1 && nIndexY >= 1);
                nStartPosX = nStartPosOrigX;
                nStartPosY = nStartPosOrigY;
                nEndPosX = dHorizMinPoints(nIndexX);
                nEndPosY = dVertMinPoints(nIndexY);
                
                nLengthX = abs(nEndPosX - nStartPosX);
                nLengthY = abs(nEndPosY - nStartPosY);
                nLength = max([nLengthX nLengthY]);
                if(nStartPosX - nLength >= 1);
                    nEndPosX = nStartPosX - nLength;
                else;
                    nStartPosX = nEndPosX + nLength;
                end;
                if(nStartPosY - nLength >= 1);
                    nEndPosY = nStartPosY - nLength;
                else;
                    nStartPosY = nEndPosY + nLength;
                end;
                
                if(dRefData(nStartPosY, nStartPosX) < 1 && dRefData(nEndPosY, nEndPosX) < 1);
                    bFound = true;
                    break;
                end;
                nIndexX = nIndexX - 1;
                nIndexX = nIndexY - 1;
            end;
        end;
    end;
    
    if(bVert);
        % left-down position
        nIndexX = nStartIndexX - 1;
        nIndexY = nStartIndexY + 1;
        while(nIndexX >= 1 &&  nIndexY <= length(dVertMinPoints));
            nStartPosX = nStartPosOrigX;
            nStartPosY = nStartPosOrigY;
            nEndPosX = dHorizMinPoints(nIndexX);
            nEndPosY = dVertMinPoints(nIndexY);

            nLengthX = abs(nEndPosX - nStartPosX);
            nLengthY = abs(nEndPosY - nStartPosY);
            nLength = max([nLengthX nLengthY]);
            if(nStartPosX - nLength >= 1);
                nEndPosX = nStartPosX - nLength;
            else;
                nStartPosX = nEndPosX + nLength;
            end;
            if(nStartPosY + nLength <= nSizeY);
                nEndPosY = nStartPosY + nLength;
            else;
                nStartPosY = nEndPosY - nLength;
            end;
            
            if(dRefData(nStartPosY, nStartPosX) < 1 && dRefData(nEndPosY, nEndPosX) < 1);
                bFound = true;
                break;
            end;
            nIndexX = nIndexX - 1;
            nIndexY = nIndexY + 1;
        end;
        
        if(bFound == false && bMeasureAll == false);
            % right-up position
            nIndexX = nStartIndexX + 1;
            nIndexY = nStartIndexY - 1;        
            while(nIndexX <= length(dHorizMinPoints) && nIndexY >= 1);
                nStartPosX = nStartPosOrigX;
                nStartPosY = nStartPosOrigY;
                nEndPosX = dHorizMinPoints(nIndexX);
                nEndPosY = dVertMinPoints(nIndexY);

                nLengthX = abs(nEndPosX - nStartPosX);
                nLengthY = abs(nEndPosY - nStartPosY);
                nLength = max([nLengthX nLengthY]);
                if(nStartPosX + nLength <= nSizeX);
                    nEndPosX = nStartPosX + nLength;
                else;
                    nStartPosX = nEndPosX - nLength;
                end;
                if(nStartPosY - nLength >= 1);
                    nEndPosY = nStartPosY - nLength;
                else;
                    nStartPosY = nEndPosY + nLength;
                end;
                
                if(dRefData(nStartPosY, nStartPosX) < 1 && dRefData(nEndPosY, nEndPosX) < 1);
                    bFound = true;
                    break;
                end;
                nIndexX = nIndexX + 1;
                nIndexX = nIndexY - 1;
            end;
        end;
    end;
end;
