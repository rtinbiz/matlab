function nMidPosY = COM_EstimateMidPosHeightByGammaFit(dX, dY, nLowPosX, nLowPosY, nMidPosX, nMidPosY, bRightPart, bShowGraph)
dX = nLowPosX - dX;
dX = dX(end:-1:1);
dY = dY - nLowPosY;
dY = dY(end:-1:1);
nMidPosX = nLowPosX - nMidPosX; 
nMidPosY = nMidPosY - nLowPosY;

%% first eliminate outlier data
nLength = length(dX);
if(nLength >= 5);
    nIndexHalf = ceil(nLength/2);
    dIndexValid = [nIndexHalf-1:nIndexHalf+1];
    dIndexCheckSequence = [];
    for(i=1:nIndexHalf);
        nIndexRight = nIndexHalf + i;
        nIndexLeft = nIndexHalf - 1 - i;
        if(nIndexRight <= nLength);
            dIndexCheckSequence = [dIndexCheckSequence nIndexRight];
        end;
        if(nIndexLeft >= 1);
            dIndexCheckSequence = [dIndexCheckSequence nIndexLeft];
        end;
    end;
    
    
    % normalize
    nMaxY = max(dY(:));
    nMinY = min(dY(:));
    dNormY = (dY - nMinY) ./ (nMaxY - nMinY);        
        
    nErrThreshold = 0.5;
    nSigmaErrThreshold = 1.4;
    for(i=1:length(dIndexCheckSequence));
        nIndexCheck = dIndexCheckSequence(i);
        dTestX = dX(dIndexValid);
        dTestY = dNormY(dIndexValid);       
        nTargetX = dX(nIndexCheck);
        nTargetY = dNormY(nIndexCheck);        
        
        nErr = COM_LineFitError(dTestX, dTestY, nTargetX, nTargetY);
 
        if(nErr < nErrThreshold);
            dIndexValid = [dIndexValid nIndexCheck];
        end;
    end; 
    
    dIndexValid = sort(dIndexValid);
    
    dX = dX(dIndexValid);
    dY = dY(dIndexValid);
 
else;
    % add mid pos to the fitting list
    dX = [dX nMidPosX];
    dY = [dY nMidPosY];
    
end;

[dAGammaFit nMeanErrorGammaFit]  = COM_GammaFit(dX, dY);
[dALogFit nMeanErrorLogFit] = COM_LogFit(dX, dY);
[dALineFit nMeanErrorLineFit] = COM_LineFit(dX, dY);

if(dAGammaFit(2) > 1);
    nMeanErrorGammaFit = 99999999;
end;

[nMin nMinIndex] = min([nMeanErrorGammaFit nMeanErrorLogFit nMeanErrorLineFit]);
switch nMinIndex(1)
case 1
    nA = dAGammaFit(1);
    nB = dAGammaFit(2);    
    nMidPosY = nA * (nMidPosX ^ nB) + nLowPosY;
    
    dEstY = nA .* (dX .^ nB);
    dX2 = [0:0.1:dX(end)];
    dEstY2 = nA .* (dX2 .^ nB);
case 2        
    nA = dALogFit(1);
    nB = dALogFit(2);

     nMidPosY = nA * log(nMidPosX) + nB + nLowPosY;

     dEstY = nA .* log(dX) + nB;
     dX2 = [0:0.1:dX(end)];
     dEstY2 = nA .* log(dX2) + nB;
case 3
    nA = dALineFit(1);
    nB = dALineFit(2);
    nMidPosY = nA * nMidPosX + nB + nLowPosY;

    dEstY = nA .* dX + nB;
    dX2 = [0:0.1:dX(end)];
    dEstY2 = nA .* dX2 + nB;
end;
    
if(bShowGraph);
    figure(401);
    if(bRightPart);
        subplot(1,2,2);
    else;
        clf;
        subplot(1,2,1);
    end;
    hold on;
    plot(dX, dY, 'r');
    plot(dX2, dEstY2, 'b');
    plot(dX, dY, 'rx');
    plot(dX, dEstY, 'bx');
    hold off;
end;