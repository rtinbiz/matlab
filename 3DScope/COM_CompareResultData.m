function [bIdentical] = COM_CompareResultData(strTitle, dSrc, dDst)
bIdentical = true;

strText = sprintf('* Compare : %s', strTitle);
disp(strText);

if(abs(dSrc.WIDTH/1000 - dDst.WIDTH)>=0.001);
    bIdentical = false;
    disp(' -> Different WIDTH');
    dDiffData = [dSrc.WIDTH dDst.WIDTH]
end;

if(abs(dSrc.WIDTH_C/1000 - dDst.WIDTH_C)>=0.001);
    bIdentical = false;
    disp(' -> Different WIDTH_C');
    dDiffData = [dSrc.WIDTH_C dDst.WIDTH_C]
end;

if(abs(dSrc.WIDTH_L/1000 - dDst.WIDTH_L)>=0.001);
    bIdentical = false;
    disp(' -> Different WIDTH_L');
    dDiffData = [dSrc.WIDTH_L dDst.WIDTH_L]
end;

if(abs(dSrc.WIDTH_R/1000 - dDst.WIDTH_R)>=0.001);
    bIdentical = false;
    disp(' -> Different WIDTH_R');
    dDiffData = [dSrc.WIDTH_R dDst.WIDTH_R]
end;

if(abs(dSrc.HEIGHT_TOTAL_L/1000 - dDst.HEIGHT_TOTAL_L)>=0.001);
    bIdentical = false;
    disp(' -> Different HEIGHT_TOTAL_L');
    dDiffData = [dSrc.HEIGHT_TOTAL_L dDst.HEIGHT_TOTAL_L]
end;

if(abs(dSrc.HEIGHT_TOTAL_R/1000 - dDst.HEIGHT_TOTAL_R)>=0.001);
    bIdentical = false;
    disp(' -> Different HEIGHT_TOTAL_R');
    dDiffData = [dSrc.HEIGHT_TOTAL_R dDst.HEIGHT_TOTAL_R]
end;

if(abs(dSrc.HEIGHT_L/1000 - dDst.HEIGHT_L)>=0.001);
    bIdentical = false;
    disp(' -> Different HEIGHT_L');
    dDiffData = [dSrc.HEIGHT_L dDst.HEIGHT_L]
end;

if(abs(dSrc.HEIGHT_E_L/1000 - dDst.HEIGHT_E_L)>=0.001);
    bIdentical = false;
    disp(' -> Different HEIGHT_E_L');
    dDiffData = [dSrc.HEIGHT_E_L dDst.HEIGHT_E_L]
end;

if(abs(dSrc.HEIGHT_R/1000 - dDst.HEIGHT_R)>=0.001);
    bIdentical = false;
    disp(' -> Different HEIGHT_R');
    dDiffData = [dSrc.HEIGHT_R dDst.HEIGHT_R]
end;

if(abs(dSrc.HEIGHT_E_R/1000 - dDst.HEIGHT_E_R)>=0.001);
    bIdentical = false;
    disp(' -> Different HEIGHT_E_R');
    dDiffData = [dSrc.HEIGHT_E_R dDst.HEIGHT_E_R]
end;

if(abs(dSrc.HEIGHT/1000 - dDst.HEIGHT)>=0.001);
    bIdentical = false;
    disp(' -> Different HEIGHT');
    dDiffData = [dSrc.HEIGHT dDst.HEIGHT]
end;

if(abs(dSrc.ANGLE_L - dDst.ANGLE_L)>=0.01);
    bIdentical = false;
    disp(' -> Different ANGLE_L');
    dDiffData = [dSrc.ANGLE_L dDst.ANGLE_L]
end;

if(abs(dSrc.ANGLE_R - dDst.ANGLE_R)>=0.01);
    bIdentical = false;
    disp(' -> Different ANGLE_R');
    dDiffData = [dSrc.ANGLE_R dDst.ANGLE_R]
end;

if(bIdentical);
    disp(' -> Identical !!');
end;
