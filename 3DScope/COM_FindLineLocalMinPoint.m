function [bFound, dCenterPoints] = COM_FindLineLocalMinPoint(dLine, nThreshold)
dPoint = [];
nBarSize = 1;

bDirDownCur = true;
bDirUpNext = false;
for i=nBarSize+1:length(dLine)-nBarSize;
    nDiffCur = dLine(i) - mean(dLine(i-nBarSize : i - 1));
    nDiffNext = mean(dLine(i+1 : i + nBarSize)) - dLine(i);
    
    if(nDiffCur < 0) bDirDownCur = true;
    elseif (nDiffCur > 0) bDirDownCur = false;
    % if nDiffCur == 0, then don't change bDirDownCur.
    end;
       
    if(nDiffNext > 0) bDirUpNext = true;
    else bDirUpNext = false;
    end;
    
    if(dLine(i) < nThreshold);
        bAddPoint = false;
        if(i == nBarSize+1 && bDirUpNext);
            bAddPoint = true;
        elseif(i == (length(dLine)-nBarSize) && ~bDirUpNext); 
            bAddPoint = true;
        elseif(bDirDownCur && bDirUpNext);
            bAddPoint = true;
        end;
        
        if(bAddPoint);
            dPoint = [dPoint i];
        end;
    end;
end;

%% combine points to one center points
dCenterPoints=[];
nMinPosX = -1;
nMaxPosX = -1;
for i=1:length(dPoint);
    nPosX = dPoint(i);
    if(nPosX <= nMaxPosX);  % existing point in current minimum
        continue;
    end;
    
    % finding boundary
    nMinPosX = nPosX;
    nMaxPosX = nPosX;
    while (dLine(nMinPosX) < nThreshold);
        nMinPosX = nMinPosX - 1; 
        
        if(nMinPosX == 0);
            break;
        end;
    end;
    while (dLine(nMaxPosX) < nThreshold);
        nMaxPosX = nMaxPosX + 1; 
        if(nMaxPosX == length(dLine) +1);
            break;
        end;
    end;
    
    if(nMinPosX == 0);
        nPosX = 1;
    elseif(nMaxPosX == length(dLine)+1);
        nPosX = length(dLine);
    else;
        nPosX = floor((nMinPosX + nMaxPosX)/2);
    end;
    
    dCenterPoints = [dCenterPoints nPosX];
end;

if(isempty(dCenterPoints));
    bFound = false;
else;
    bFound = true;
end;

