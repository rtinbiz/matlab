function [dRefData, dRefMeanLineData] = DOT_GetRefHeightData(dHeightData)
[nSizeY nSizeX] = size(dHeightData);
dRefData = zeros(nSizeY, nSizeX);
nMean = mean(dHeightData(:));
nThreshold = floor(nMean * 0.8);

dIndex = find(dHeightData > nThreshold);
dRefData(dIndex) = 1;

dRefMeanLineData = ~dRefData;
dLabel = bwlabel(dRefMeanLineData);
dStat = regionprops(dLabel, 'all');

nCount = length(dStat);
if(nCount > 1);
    dArea = zeros(nCount, 1);
    nIndex = 1;
    for(i=1:nCount);
        if(dStat(i).Area > 100);
            dArea(nIndex) = dStat(i).Area;
            nIndex = nIndex + 1;
        end;
    end;
    dArea = sort(dArea);
    nCount = length(dArea);
    nMidAreaSize = dArea(floor(2*nCount/3));
    nRadius = sqrt(nMidAreaSize/pi);
    nFilterSize = floor(nRadius/5);
    se = strel('disk',nFilterSize);
    dRefMeanLineData = imerode(dRefMeanLineData, se);
end;

dRefMeanLineData = ~dRefMeanLineData;


