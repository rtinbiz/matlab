function nPos = COM_GetCornerPos(dLine, dParameter, bReverse)

umPerPixel = dParameter.nmPerPixel / 1000;
umPerHeight = dParameter.nmPerHeight / 1000;

nLength = length(dLine);
dAngleProfile = ones(1, nLength) * 180;

if(bReverse);
    dLine = dLine(nLength:-1:1);
end;

% scaling
% nMax = max(dLine(:));
% nMin = min(dLine(:));
% 
% dLine = ((dLine - nMin) / (nMax - nMin)) * nLength;
% 
nAngleThreshold = 175;
nBarSize = 1;
for i=nBarSize+1:nLength-nBarSize-1;
    nLeftPosStart = i-nBarSize;
    nLeftPosEnd = i-1;
    if(nLeftPosStart < 1) nLeftPosStart = 1; end;
    if(nLeftPosEnd < 1) nLeftPosEnd = 1; end;

    nRightPosStart = i+1;
    nRightPosEnd = i+nBarSize;
    if(nRightPosStart > nLength) nRightPosStart = nLength; end;
    if(nRightPosEnd > nLength) nRightPosEnd = nLength; end;
    
    nLeftPosX = nLeftPosStart;
    nLeftPosY = dLine(nLeftPosX); %mean(dLine(nLeftPosStart:nLeftPosEnd));
    nCenterPosX = i;
    nCenterPosY = dLine(nCenterPosX);
    nRightPosX = nRightPosEnd;
    nRightPosY = dLine(nRightPosX); %mean(dLine(nRightPosStart:nRightPosEnd));
    
    nAngle = COM_CalcAngle(nLeftPosX, nLeftPosY, nCenterPosX, nCenterPosY, nRightPosX, nRightPosY, umPerPixel, umPerHeight);
    
    dAngleProfile(i) = nAngle;
end;

dAnglePointIndex = find(dAngleProfile > nAngleThreshold);
dAngleProfile(dAnglePointIndex) = 180;

[nMin, nPos] = min(dAngleProfile(:));

if(bReverse);
    nPos = nLength - nPos + 1;
end;


figure(404); 
clf;
subplot(2,1,1); plot(dLine);
subplot(2,1,2); plot(dAngleProfile);
