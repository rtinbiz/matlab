function [bFound nPosX nPosY dHorizMeanLine dHorizCenterPoints] = STR_FindStartCenterPoint(dData, dRefData, bShowGraph)
[nSizeY nSizeX] = size(dData);
nHeight = max(dData(:));
nRefDataHeight = max(dRefData(:));

%% Filter for Line Smoothing
nFilterSize=25;
dFilter=(1/nFilterSize)*ones(1, nFilterSize);

%% Get vertical line data and x position
dHorizMeanLine = dRefData;
dHorizMeanLine=imfilter(dHorizMeanLine,dFilter,'replicate','conv');

nLocalMinThreshold = 0.7;

[bFound, dHorizCenterPoints] = COM_FindLineLocalMinPoint(dHorizMeanLine, nLocalMinThreshold);
if(bFound == false);
    return;
end;

dDiffCenter = abs(dHorizCenterPoints - nSizeX/2);
nIndex = find(dDiffCenter == min(dDiffCenter));
nPosX = dHorizCenterPoints(nIndex);
nMinPosX = nPosX;
nMaxPosX = nPosX;
while (dHorizMeanLine(nMinPosX) < nLocalMinThreshold) && (nMinPosX > 1);
    nMinPosX = nMinPosX - 1; 
end;
while (dHorizMeanLine(nMaxPosX) < nLocalMinThreshold) && (nMaxPosX < nSizeX);
    nMaxPosX = nMaxPosX + 1; 
end;

%%  Get y position (center pos)
nPosY = floor(nSizeY/2);

bFound = true;

%% Show Graph
if bShowGraph; 
    figure(101);
    clf;
    subplot(2,1,1);
    mesh(dData);
    axis([0 nSizeX 0 nSizeY 0 nHeight]);
    view([0 -90]);
    hold on;
    plot(nPosX, nPosY, 'rx');
    text(nPosX, nPosY + 70, 100, 'Start Point', 'Horizontalalignment','center');
    hold off;

    subplot(2,1,2);
    hold on;
    plot(dHorizMeanLine);
    plot(dHorizCenterPoints, dHorizMeanLine(dHorizCenterPoints), 'rx')    
    axis([0 nSizeX 0 nRefDataHeight]);
    hold off;
    
    drawnow;
end;

