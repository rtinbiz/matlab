function COM_ShowMeasureResult(dMeasure, bMeasureAll, szAngle, dParameter)
bDiagonal = false;
szAngle=lower(szAngle);
if ~isempty(strfind(szAngle,'d'));
    bDiagonal = true;
end;

umPerPixel = dParameter.nmPerPixel / 1000;
if(bDiagonal);
    umPerPixel= umPerPixel * sqrt(2);
end;
umPerHeight = dParameter.nmPerHeight / 1000;
umSpaceGap = dParameter.nGlassSpaceGap / 1000;

szFileName = 'MeasureDataBack.bmp';
dDataBackImage = imread(szFileName);

nCount = length(dMeasure);

figure(200);

nPoxX = 200;
nPoxY = 80;
set(gcf,'position',[nPoxX nPoxY (nPoxX+800) (nPoxY+800)]);

for(i=1:nCount);
%    dIndexCheck = [108];
    dIndexCheck = [];
    if(~isempty(dIndexCheck) && isempty(find(dIndexCheck==i)));
        continue;
    end;
    
    dInverseHeightProfile = dMeasure(i).dInverseHeightProfile;
    dAngleMinPoints= dMeasure(i).dAngleMinPoints;
    nHighPos_L = dMeasure(i).dFeaturePointLeft(1);
    nMidPos_L = dMeasure(i).dFeaturePointLeft(2);
    nLowPos_L = dMeasure(i).dFeaturePointLeft(3);
    nLowPos_R = dMeasure(i).dFeaturePointRight(1);
    nMidPos_R = dMeasure(i).dFeaturePointRight(2);
    nHighPos_R = dMeasure(i).dFeaturePointRight(3);
    dResult = dMeasure(i).dResult;
    
    dFeaturePoint = [nHighPos_L.x nLowPos_L.x nLowPos_R.x nHighPos_R.x];
    dFeaturePointHeight = [nHighPos_L.y nLowPos_L.y nLowPos_R.y nHighPos_R.y];
    dMidFeaturePoint = [nMidPos_L.x nMidPos_R.x];
    dMidFeaturePointHeight = [nMidPos_L.y nMidPos_R.y];
    
    clf;
    subplot(2,1,1);
    hold on;
    plot(dInverseHeightProfile);
    dAngleHeight = dInverseHeightProfile(dAngleMinPoints);,
    plot(dAngleMinPoints, dAngleHeight, 'mx');
    plot(dFeaturePoint, dFeaturePointHeight, 'r*');
    plot(dMidFeaturePoint, dMidFeaturePointHeight, 'g*');    
    nHeightMargin = (max(dInverseHeightProfile) - min(dInverseHeightProfile)) * 0.1;
    axis([0 length(dInverseHeightProfile) (min(dInverseHeightProfile)-nHeightMargin) (max(dInverseHeightProfile)+nHeightMargin)]);
    hold off;
    
    subplot(2, 1, 2);
    imshow(dDataBackImage);

    nPosX = 10;
    nPosY = 290;
    nLineHeight = 20;

    nPosY = nPosY + nLineHeight;
    szText = sprintf('Index : %d/%d', i, nCount); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','left');

    nPosX = 370;
    nPosY = 25;
    szText = sprintf('(1) %.3f um', dResult.WIDTH); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','center');

    nPosX = 370;
    nPosY = 245;
    szText = sprintf('(2) %.3f um', dResult.WIDTH_C); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','center');

    nPosX = 370 - 150;
    nPosY = 245;
    szText = sprintf('(3) %.3f um', dResult.WIDTH_L); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','center');

    nPosX = 370 + 150;
    nPosY = 245;
    szText = sprintf('(4) %.3f um', dResult.WIDTH_R); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','center');

    nPosX = 370 - 280;
    nPosY = 170;
    szText = sprintf('(5) %.3f um', dResult.HEIGHT_L); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','center');

    nPosX = 370 - 280;
    nPosY = 65;
    szText = sprintf('(6) %.3f um', dResult.HEIGHT_E_L); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','center');

    nPosX = 370 + 280;
    nPosY = 170;
    szText = sprintf('(7) %.3f um', dResult.HEIGHT_R); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','center');

    nPosX = 370 + 280;
    nPosY = 65;
    szText = sprintf('(8) %.3f um', dResult.HEIGHT_E_R); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','center');

    nPosX = 370 - 180;
    nPosY = 90;
    szText = sprintf('(9) %.3f ''', dResult.ANGLE_L); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','center');

    nPosX = 370 + 180;
    nPosY = 90;
    szText = sprintf('(10) %.3f ''', dResult.ANGLE_R); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','center');

    nPosX = 370 - 310;
    nPosY = 120;
    szText = sprintf('(11) %.3f um', dResult.HEIGHT_TOTAL_L); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','center');

    nPosX = 370 + 310;
    nPosY = 120;
    szText = sprintf('(12) %.3f um', dResult.HEIGHT_TOTAL_R); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','center');

    drawnow;
    pause;
end;


