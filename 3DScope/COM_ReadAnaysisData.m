function [dAnalysis] = COM_ReadAnalysisData(szDataPath, szFileName)
szFilePath=sprintf('%s%s_A.csv',szDataPath,szFileName);   
nFID = fopen(szFilePath);
if(nFID == -1);
    disp('Fail to open data file.');
    return;
end;

while 1
    strLine = fgetl(nFID);
    if ~ischar(strLine);
        break;
    end;

    dIndex = findstr(strLine, ',');
    strId = strLine(1:dIndex(1)-1);
    nCount = str2num(strLine(dIndex(1)+1:dIndex(2)-1));
    dContainer = textscan(strLine(dIndex(2)+1:end),'%f','Delimiter',',');
    dData = dContainer{1}';
    
    dIndex = findstr(strId, '_');
    if ~isempty(dIndex);
        nIndex = str2num(strId(dIndex(1)+1:end));
        strId = strId(1:dIndex(1)-1);
    end;
    
    % Parameters
    if(strcmp(strId, 'NPP')); dAnalysis.nmPerPixel = dData;
    elseif(strcmp(strId, 'NPH')); dAnalysis.nmPerHeight = dData;
    elseif(strcmp(strId, 'THK')); dAnalysis.nTargetThick = dData;
    elseif(strcmp(strId, 'NSG')); dAnalysis.nGlassSpaceGap = dData;
    elseif(strcmp(strId, 'MST')); dAnalysis.nMaskType = dData;
    elseif(strcmp(strId, 'AGL')); dAnalysis.nMeasureAngle = dData;
    elseif(strcmp(strId, 'ALL')); dAnalysis.bMeasureAll = dData;
    % Data for measuring
    elseif(strcmp(strId, 'REF')); dAnalysis.dRefImage = dData;
    elseif(strcmp(strId, 'HML')); dAnalysis.dHorizMeanLine = dData;
    elseif(strcmp(strId, 'HMP')); dAnalysis.dHorizMinCenterPoints = dData;
    elseif(strcmp(strId, 'VML')); dAnalysis.dVertMeanLine = dData;
    elseif(strcmp(strId, 'VMP')); dAnalysis.dVertMinCenterPoints = dData;
    % Proc Data    
    elseif(strcmp(strId, 'SP')); dAnalysis.dMeasure(nIndex).dStartPoint = dData;
    elseif(strcmp(strId, 'EP')); dAnalysis.dMeasure(nIndex).dEndPoint = dData;
    elseif(strcmp(strId, 'HF')); dAnalysis.dMeasure(nIndex).dHeightProfile = dData;
    elseif(strcmp(strId, 'IHF')); dAnalysis.dMeasure(nIndex).dInverseHeightProfile = dData;
    elseif(strcmp(strId, 'AF')); dAnalysis.dMeasure(nIndex).dAngleProfile = dData;
    elseif(strcmp(strId, 'AP')); dAnalysis.dMeasure(nIndex).dAngleMinPoints = dData;
    elseif(strcmp(strId, 'FPL')); dAnalysis.dMeasure(nIndex).dFeaturePointLeft = dData;
    elseif(strcmp(strId, 'FPR')); dAnalysis.dMeasure(nIndex).dFeaturePointRight= dData;
    % Result
    elseif(strcmp(strId, 'MW')); dAnalysis.dMeasure(nIndex).dResult.WIDTH = dData;
    elseif(strcmp(strId, 'MWC')); dAnalysis.dMeasure(nIndex).dResult.WIDTH_C = dData;
    elseif(strcmp(strId, 'MWL')); dAnalysis.dMeasure(nIndex).dResult.WIDTH_L = dData;
    elseif(strcmp(strId, 'MWR')); dAnalysis.dMeasure(nIndex).dResult.WIDTH_R = dData;
    elseif(strcmp(strId, 'MHT')); dAnalysis.dMeasure(nIndex).dResult.HEIGHT = dData;
    elseif(strcmp(strId, 'MHTL')); dAnalysis.dMeasure(nIndex).dResult.HEIGHT_TOTAL_L = dData;
    elseif(strcmp(strId, 'MHTR')); dAnalysis.dMeasure(nIndex).dResult.HEIGHT_TOTAL_R = dData;
    elseif(strcmp(strId, 'MHL')); dAnalysis.dMeasure(nIndex).dResult.HEIGHT_L = dData;
    elseif(strcmp(strId, 'MHEL')); dAnalysis.dMeasure(nIndex).dResult.HEIGHT_E_L = dData;
    elseif(strcmp(strId, 'MHR')); dAnalysis.dMeasure(nIndex).dResult.HEIGHT_R = dData;
    elseif(strcmp(strId, 'MHER')); dAnalysis.dMeasure(nIndex).dResult.HEIGHT_E_R = dData;
    elseif(strcmp(strId, 'MAL')); dAnalysis.dMeasure(nIndex).dResult.ANGLE_L = dData;
    elseif(strcmp(strId, 'MAR')); dAnalysis.dMeasure(nIndex).dResult.ANGLE_R = dData;
    end;
end;
fclose(nFID);
