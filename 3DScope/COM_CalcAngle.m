function nAngle = COM_CalcAngle(nLeftPosX, nLeftPosY, nCenterPosX, nCenterPosY, nRightPosX, nRightPosY, umPerPixel, umPerHeight)

nPoint1_X = (nLeftPosX - nCenterPosX) * umPerPixel;
nPoint1_Y = (nLeftPosY - nCenterPosY) * umPerHeight;
nPoint2_X = (nRightPosX - nCenterPosX) * umPerPixel;
nPoint2_Y = (nRightPosY - nCenterPosY) * umPerHeight;

nAngle = acos((nPoint1_X*nPoint2_X + nPoint1_Y*nPoint2_Y)/ ...
    (sqrt(nPoint1_X^2 + nPoint1_Y^2)*sqrt(nPoint2_X^2 + nPoint2_Y^2))) * (180 / pi);
