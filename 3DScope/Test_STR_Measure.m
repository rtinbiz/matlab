clear all;
close all;

%% Data Path
szDataPath = 'D:\Z\Measer3D\AnalysisData\';

%% LINE Data
%szFileName = 'NewData_2012_07_26_10_02_57_STR'; 
%szFileName = 'NewData_2012_07_26_09_56_06_STR'; 
%szFileName = 'NewData_2012_07_26_09_56_06_20141109-085356';
szFileName = 'STRIPE_31_2014_11_24_21_42_50';
%%

dAnalysis = COM_ReadAnaysisData(szDataPath, szFileName);
dHeightData = COM_ReadHeightData(szDataPath, szFileName, false);

% for test
dParameter.nmPerPixel = dAnalysis.nmPerPixel;
dParameter.nmPerHeight = dAnalysis.nmPerHeight;
dParameter.nTargetThick = dAnalysis.nTargetThick/1000;
dParameter.nGlassSpaceGap = dAnalysis.nGlassSpaceGap;

if (dAnalysis.nMeasureAngle == 1); szAngle = 'd';
else; szAngle = 'n'; end;

if (dAnalysis.bMeasureAll == 1); bMeasureAll = true;
else; bMeasureAll = false; end;

[bSuccess dRun] = STR_Measure(dHeightData, dParameter);
