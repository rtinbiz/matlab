function dData = COM_SmoothingHeightData(dHeightData, bShowGraph)
dData = dHeightData;
[nSizeY nSizeX] = size(dData);
nHeight = max(dData(:));

%% Smoothing
nFilterSize=5;
dFilter=(1/(nFilterSize^2))*ones(nFilterSize, nFilterSize);
dData=imfilter(dData,dFilter,'replicate','conv');
% 
% nFilterSize=5;
% se = strel('disk',nFilterSize);
% dData = imclose(dData, se);

%% Show Graph
if bShowGraph; 
    figure(91);
    clf;
    subplot(1,2,1);
    mesh(dHeightData);
    axis([0 nSizeX 0 nSizeY 0 nHeight]);
    view([0 -90]);
  
    subplot(1,2,2);
    mesh(dData);
    axis([0 nSizeX 0 nSizeY 0 nHeight]);
    view([0 -90]);
      
    drawnow;
end;
