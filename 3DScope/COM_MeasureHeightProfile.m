function [dResult dInverseHeightProfile dAngleProfile dAnglePoint dFeaturePoint_L dFeaturePoint_R] = COM_MeasureHeightProfile(dHeightProfile, szAngle, dParameter, bShowGraph)
nLength = length(dHeightProfile);

bDiagonal = false;
szAngle=lower(szAngle);
if ~isempty(strfind(szAngle,'d'));
    bDiagonal = true;
end;

umPerPixel = dParameter.nmPerPixel / 1000;
umPerDiagPixel = umPerPixel * sqrt(2);
umPerHeight = dParameter.nmPerHeight / 1000;
umSpaceGap = dParameter.nGlassSpaceGap / 1000;

umPerPixelForMeasure = umPerPixel;
if(bDiagonal);
    umPerPixelForMeasure = umPerDiagPixel;
end;
   
dHeightProfile = max(dHeightProfile) - dHeightProfile;

%% Smoothing
% nFilterSize=3;
%dFilter=(1/nFilterSize)*ones(1, nFilterSize);
%dInverseHeightProfile=imfilter(dHeightProfile,dFilter,'replicate','conv');
dInverseHeightProfile = dHeightProfile;
% 
% nFilterSize=3;
% se = strel('line',nFilterSize,0);
% dInverseHeightProfile = imclose(dInverseHeightProfile, se);
% dInverseHeightProfile = imopen(dInverseHeightProfile, se);
dProfile = dInverseHeightProfile;

nAngleThreshold = 175;
dAngleProfile = COM_GetAngleProfile(dProfile, umPerPixelForMeasure, umPerHeight, nAngleThreshold);
dAnglePoint = COM_FindAngleLocalMinPoint(dProfile, dAngleProfile, nAngleThreshold);
 
dFeaturePoint_L = COM_SelectFeaturePoint(dProfile, dAngleProfile, dAnglePoint, umPerPixelForMeasure, umPerHeight, false);
dFeaturePoint_R = COM_SelectFeaturePoint(dProfile, dAngleProfile, dAnglePoint, umPerPixelForMeasure, umPerHeight, true);

nHighPos_L = dFeaturePoint_L.nHighPos;
nMidPos_L = dFeaturePoint_L.nMidPos;
nLowPos_L = dFeaturePoint_L.nLowPos;
nHighPos_L_Height = dFeaturePoint_L.nHighPosHeight;
nMidPos_L_Height = dFeaturePoint_L.nMidPosHeight;
nLowPos_L_Height = dFeaturePoint_L.nLowPosHeight;
nHighPos_R = dFeaturePoint_R.nHighPos;
nMidPos_R = dFeaturePoint_R.nMidPos;
nLowPos_R = dFeaturePoint_R.nLowPos;
nHighPos_R_Height = dFeaturePoint_R.nHighPosHeight;
nMidPos_R_Height = dFeaturePoint_R.nMidPosHeight;
nLowPos_R_Height = dFeaturePoint_R.nLowPosHeight;

%% Calculate Distance
nMeasureData_WIDTH = (abs(nMidPos_R - nMidPos_L) + 1) * umPerPixelForMeasure;
nMeasureData_WIDTH_C = (abs(nLowPos_R - nLowPos_L) + 1) * umPerPixelForMeasure;
nMeasureData_WIDTH_L = (abs(nMidPos_L - nLowPos_L) + 1) * umPerPixelForMeasure;
nMeasureData_WIDTH_R = (abs(nMidPos_R - nLowPos_R) + 1) * umPerPixelForMeasure;
nMeasureData_HEIGHT_TOTAL_L = (abs(nHighPos_L_Height - nLowPos_L_Height) + 1) * umPerHeight - umSpaceGap;
nMeasureData_HEIGHT_TOTAL_R = (abs(nHighPos_R_Height - nLowPos_R_Height) + 1) * umPerHeight - umSpaceGap;
nMeasureData_HEIGHT_L = (abs(nMidPos_L_Height - nLowPos_L_Height) + 1) * umPerHeight;
nMeasureData_HEIGHT_R = (abs(nMidPos_R_Height - nLowPos_R_Height) + 1) * umPerHeight;
nMeasureData_HEIGHT_E_L = nMeasureData_HEIGHT_TOTAL_L - nMeasureData_HEIGHT_L;
nMeasureData_HEIGHT_E_R = nMeasureData_HEIGHT_TOTAL_R - nMeasureData_HEIGHT_R;
nMeasureData_HEIGHT = nMeasureData_HEIGHT_TOTAL_L;
if(nMeasureData_HEIGHT_TOTAL_R > nMeasureData_HEIGHT_TOTAL_L);
    nMeasureData_HEIGHT = nMeasureData_HEIGHT_TOTAL_R;
end;

nPoint1_X = (nLowPos_L - nMidPos_L) * umPerPixelForMeasure;
nPoint1_Y = 0;
nPoint2_X = nPoint1_X;
nPoint2_Y = (nLowPos_L_Height - nMidPos_L_Height) * umPerHeight;
nMeasureData_ANGLE_L = acos((nPoint1_X*nPoint2_X + nPoint1_Y*nPoint2_Y)/ ...
    (sqrt(nPoint1_X^2 + nPoint1_Y^2)*sqrt(nPoint2_X^2 + nPoint2_Y^2))) * (180 / pi);

nPoint1_X = (nLowPos_R - nMidPos_R) * umPerPixelForMeasure;
nPoint1_Y = 0;
nPoint2_X = nPoint1_X;
nPoint2_Y = (nLowPos_R_Height - nMidPos_R_Height) * umPerHeight;
nMeasureData_ANGLE_R = acos((nPoint1_X*nPoint2_X + nPoint1_Y*nPoint2_Y)/ ...
    (sqrt(nPoint1_X^2 + nPoint1_Y^2)*sqrt(nPoint2_X^2 + nPoint2_Y^2))) * (180 / pi);

dResult.WIDTH = nMeasureData_WIDTH;
dResult.WIDTH_C = nMeasureData_WIDTH_C;
dResult.WIDTH_L = nMeasureData_WIDTH_L;
dResult.WIDTH_R = nMeasureData_WIDTH_R;
dResult.HEIGHT_TOTAL_L = nMeasureData_HEIGHT_TOTAL_L;
dResult.HEIGHT_TOTAL_R = nMeasureData_HEIGHT_TOTAL_R;
dResult.HEIGHT_L = nMeasureData_HEIGHT_L;
dResult.HEIGHT_E_L = nMeasureData_HEIGHT_E_L;
dResult.HEIGHT_R = nMeasureData_HEIGHT_R;
dResult.HEIGHT_E_R = nMeasureData_HEIGHT_E_R;
dResult.HEIGHT = nMeasureData_HEIGHT;
dResult.ANGLE_L = nMeasureData_ANGLE_L;
dResult.ANGLE_R = nMeasureData_ANGLE_R;

%%
if bShowGraph; 
    figure(131);
    clf;
    nPoxX = 100;
    nPoxY = 200;
    set(gcf,'position',[nPoxX nPoxY (nPoxX+500) (nPoxY+400)]);

    dFeaturePoint = [nHighPos_L nLowPos_L nLowPos_R nHighPos_R];
    dFeaturePointHeight = [nHighPos_L_Height nLowPos_L_Height nLowPos_R_Height nHighPos_R_Height];
    dMidFeaturePoint = [nMidPos_L nMidPos_R];
    dMidFeaturePointHeight = [nMidPos_L_Height nMidPos_R_Height];    
    
    subplot(2,1,1);
    hold on;
    plot(dHeightProfile);
    dAngleHeight = dProfile(dAnglePoint);,
    plot(dAnglePoint, dAngleHeight, 'mx');
    plot(dFeaturePoint, dFeaturePointHeight, 'r*');
    plot(dMidFeaturePoint, dMidFeaturePointHeight, 'g*');        
    nHeightMargin = (max(dHeightProfile) - min(dHeightProfile)) * 0.1;
    axis([0 nLength (min(dHeightProfile)-nHeightMargin) (max(dHeightProfile)+nHeightMargin)]);
    hold off;

%     subplot(2,2,3);
%     hold on;
%     plot(dProfile);
%     plot(dAnglePoint, dAngleHeight, 'mx');
%     plot(dFeaturePoint, dFeaturePointHeight, 'r*');
%     plot(dMidFeaturePoint, dMidFeaturePointHeight, 'g*');    
%     nHeightMargin = (max(dProfile) - min(dProfile)) * 0.1;
%     axis([0 nLength (min(dProfile)-nHeightMargin) (max(dProfile)+nHeightMargin)]);
%     hold off;

    subplot(2,1,2);
    plot(dAngleProfile,'r');
    nHeightMargin = (max(dAngleProfile) - min(dAngleProfile)) * 0.1;
    axis([0 nLength (min(dAngleProfile)-nHeightMargin) (max(dAngleProfile)+nHeightMargin)]);

%     subplot(2,2,4);
%     plot(dSmoothAngleProfile,'r');
%     nHeightMargin = (max(dSmoothAngleProfile) - min(dSmoothAngleProfile)) * 0.1;
%     axis([0 nLength (min(dSmoothAngleProfile)-nHeightMargin) (max(dSmoothAngleProfile)+nHeightMargin)]);
    
    figure(132);
    clf;
    nPoxX = 620;
    nPoxY = 50;
    set(gcf,'position',[nPoxX nPoxY (nPoxX+500) (nPoxY+300)]);
    
    szFileName = 'MeasureDataBack.bmp';
    imshow(imread(szFileName));

    nPosX = 10;
    nPosY = 290;
    nLineHeight = 20;
    
    szText = sprintf('umPerPixel : %.6f um (Diagonal : %.6f um)', umPerPixel, umPerDiagPixel); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','left');

    nPosY = nPosY + nLineHeight;
    szText = sprintf('umPerHeight : %.6f um', umPerHeight); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','left');

    nPosX = 370;
    nPosY = 25;
    szText = sprintf('(1) %.3f um', nMeasureData_WIDTH); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','center');

    nPosX = 370;
    nPosY = 245;
    szText = sprintf('(2) %.3f um', nMeasureData_WIDTH_C); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','center');

    nPosX = 370 - 150;
    nPosY = 245;
    szText = sprintf('(3) %.3f um', nMeasureData_WIDTH_L); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','center');

    nPosX = 370 + 150;
    nPosY = 245;
    szText = sprintf('(4) %.3f um', nMeasureData_WIDTH_R); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','center');
    
    nPosX = 370 - 280;
    nPosY = 170;
    szText = sprintf('(5) %.3f um', nMeasureData_HEIGHT_L); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','center');

    nPosX = 370 - 280;
    nPosY = 65;
    szText = sprintf('(6) %.3f um', nMeasureData_HEIGHT_E_L); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','center');
    
    nPosX = 370 + 280;
    nPosY = 170;
    szText = sprintf('(7) %.3f um', nMeasureData_HEIGHT_R); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','center');

    nPosX = 370 + 280;
    nPosY = 65;
    szText = sprintf('(8) %.3f um', nMeasureData_HEIGHT_E_R); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','center');

    nPosX = 370 - 180;
    nPosY = 90;
    szText = sprintf('(9) %.3f ''', nMeasureData_ANGLE_L); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','center');

    nPosX = 370 + 180;
    nPosY = 90;
    szText = sprintf('(10) %.3f ''', nMeasureData_ANGLE_R); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','center');

    nPosX = 370 - 310;
    nPosY = 120;
    szText = sprintf('(11) %.3f um', nMeasureData_HEIGHT_TOTAL_L); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','center');

    nPosX = 370 + 310;
    nPosY = 120;
    szText = sprintf('(12) %.3f um', nMeasureData_HEIGHT_TOTAL_R); 
    text(nPosX, nPosY, szText, 'Horizontalalignment','center');
    
    drawnow;
end;
