clear all;
close all;

%% Data Path
szDataPath = '..\..\Data\TestData\';

%% DOT data
nIndex=0; 
nIndex=nIndex+1; dTestSet(nIndex).szFileName = 'NewData_2012_07_25_18_23_25_DOT'; 
nIndex=nIndex+1; dTestSet(nIndex).szFileName = 'NewData_Y2012_M00_D23_H10_M00_S56_DOT'; 
nIndex=nIndex+1; dTestSet(nIndex).szFileName = 'NewData_Y2012_M00_D23_H06_M00_S26_DOT';
nIndex=nIndex+1; dTestSet(nIndex).szFileName = 'NewData_Y2012_M14_D24_H07_M14_S27_DOT';
nIndex=nIndex+1; dTestSet(nIndex).szFileName = '00_DOT';
nIndex=nIndex+1; dTestSet(nIndex).szFileName = '01_DOT';
nIndex=nIndex+1; dTestSet(nIndex).szFileName = '02_DOT';
nIndex=nIndex+1; dTestSet(nIndex).szFileName = '03_DOT';
nIndex=nIndex+1; dTestSet(nIndex).szFileName = '04_DOT';
nIndex=nIndex+1; dTestSet(nIndex).szFileName = '00-50x_DOT';
%%

nCount = length(dTestSet);
for d=1:2;
    if(d == 1);
        szDir = 'n'; 
        fprintf('\n==== Normal Measure ====');
    elseif(d == 2) 
        szDir = 'd'; 
        fprintf('\n==== Diagonal Measure ====');
    end;
        
    for i=1:nCount;
        szFileName = dTestSet(i).szFileName;

        fprintf('\n(%d/%d) : %s', i, nCount, szFileName);

        dParameter = COM_ReadCSVData(szDataPath, szFileName, 'P', false);
        dHeightData = COM_ReadCSVData(szDataPath, szFileName, 'H', false);
        %dLaserData = COM_ReadCSVData(szDataPath, szFileName, 'L', true);
        %dColorData = COM_ReadCSVData(szDataPath, szFileName, 'C', true);
        %dLaserColorData = COM_ReadCSVData(szDataPath, szFileName, 'LC', true);

        % for test
        nMeasureData_HEIGHT_TOTAL_L = 1;
        nMeasureData_HEIGHT_TOTAL_R = 1;
        dParameter = [dParameter nMeasureData_HEIGHT_TOTAL_L nMeasureData_HEIGHT_TOTAL_R];

        [bSuccess dMeasureData] = DOT_Measure(dHeightData, szDir, dParameter);
        if(bSuccess);    
            nMeasureData_WIDTH = dMeasureData(1);
            nMeasureData_WIDTH_C = dMeasureData(2);
            nMeasureData_WIDTH_L = dMeasureData(3);
            nMeasureData_WIDTH_R = dMeasureData(4);
            nMeasureData_HEIGHT_L = dMeasureData(5);
            nMeasureData_HEIGHT_E_L = dMeasureData(6);
            nMeasureData_HEIGHT_R = dMeasureData(7);
            nMeasureData_HEIGHT_E_R = dMeasureData(8);
            nMeasureData_ANGLE_L = dMeasureData(9);
            nMeasureData_ANGLE_R = dMeasureData(10);
        end;

        pause;
    end;
end;