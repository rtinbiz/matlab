clear all;
close all;

%% Data Path
szDataPath = '..\..\Data\TestData\';

%% DOT data
nIndex=0; 
nIndex=nIndex+1; dTestSet(nIndex).szFileName = 'NewData_2012_07_26_10_02_57_STR'; 
nIndex=nIndex+1; dTestSet(nIndex).szFileName = 'NewData_2012_07_26_09_56_06_STR'; 
%%

nCount = length(dTestSet);
for i=1:nCount;
    szFileName = dTestSet(i).szFileName;

    fprintf('\n(%d/%d) : %s', i, nCount, szFileName);

    dParameter = COM_ReadCSVData(szDataPath, szFileName, 'P', false);
    dHeightData = COM_ReadCSVData(szDataPath, szFileName, 'H', false);
    %dLaserData = COM_ReadCSVData(szDataPath, szFileName, 'L', true);
    %dColorData = COM_ReadCSVData(szDataPath, szFileName, 'C', true);
    %dLaserColorData = COM_ReadCSVData(szDataPath, szFileName, 'LC', true);

    % for test
    nMeasureData_HEIGHT_TOTAL_L = 1;
    nMeasureData_HEIGHT_TOTAL_R = 1;
    dParameter = [dParameter nMeasureData_HEIGHT_TOTAL_L nMeasureData_HEIGHT_TOTAL_R];

    dData = dHeightData;
    [bSuccess dMeasureData] = STR_Measure(dData, dHeightData, dParameter);
    if(bSuccess);    
        nMeasureData_WIDTH = dMeasureData(1);
        nMeasureData_WIDTH_C = dMeasureData(2);
        nMeasureData_WIDTH_L = dMeasureData(3);
        nMeasureData_WIDTH_R = dMeasureData(4);
        nMeasureData_HEIGHT_L = dMeasureData(5);
        nMeasureData_HEIGHT_E_L = dMeasureData(6);
        nMeasureData_HEIGHT_R = dMeasureData(7);
        nMeasureData_HEIGHT_E_R = dMeasureData(8);
        nMeasureData_ANGLE_L = dMeasureData(9);
        nMeasureData_ANGLE_R = dMeasureData(10);
    end;

    pause;
end;