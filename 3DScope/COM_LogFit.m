function [dA nMeanErr] = COM_LogFit(dX, dY);
dLogX = log(dX);
dA = COM_LineFit(dLogX, dY);
nA = dA(1);
nB = dA(2);
dEstY = nA .* log(dX) + nB;
nMeanErr = mean(abs(dEstY - dY));
