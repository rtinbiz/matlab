function dAngleProfile = COM_GetAngleProfile(dProfile, umPerPixel, umPerHeight, nAngleThreshold)
nLength = length(dProfile);
dAngleProfile = ones(1, nLength) * 180;

nBarSize = 1;
for i=1:nLength;
    if(i==1 || i==nLength);
        continue;
    end;
    
    nLeftPosStart = i-nBarSize;
    nLeftPosEnd = i-1;
    if(nLeftPosStart < 1) nLeftPosStart = 1; end;
    if(nLeftPosEnd < 1) nLeftPosEnd = 1; end;

    nRightPosStart = i+1;
    nRightPosEnd = i+nBarSize;
    if(nRightPosStart > nLength) nRightPosStart = nLength; end;
    if(nRightPosEnd > nLength) nRightPosEnd = nLength; end;
    
    nLeftPosX = mean(nLeftPosStart:nLeftPosEnd);
    nLeftPosY = dProfile(nLeftPosX);
    nCenterPosX = i;
    nCenterPosY = dProfile(nCenterPosX);
    nRightPosX = mean(nRightPosStart:nRightPosEnd);
    nRightPosY = dProfile(nRightPosX);
    
    nAngle = COM_CalcAngle(nLeftPosX, nLeftPosY, nCenterPosX, nCenterPosY, nRightPosX, nRightPosY, umPerPixel, umPerHeight);
    
    dAngleProfile(i) = nAngle;
end;

dAnglePointIndex = find(dAngleProfile > nAngleThreshold);
dAngleProfile(dAnglePointIndex) = 180;
