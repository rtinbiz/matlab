function [bIdentical] = COM_CompareData(strTitle, dSrc, dDst, bPoint)
bIdentical = true;

nLengthSrc = length(dSrc);
nLengthDst = length(dDst);

if(nLengthSrc ~= nLengthDst);
    bIdentical = false;
    
    strText = sprintf('* Compare : %s -> Different length !!, Analysis Data(%d), Measure Data(%d)', strTitle, nLengthSrc, nLengthDst);
    disp(strText);
    return;
end;

if bPoint;
    dSrc = dSrc +1;
end;

dDiff = abs(dSrc - dDst);
dIndex = find(dDiff >= 0.01);
if(isempty(dIndex));
    strText = sprintf('* Compare : %s -> Identical !!', strTitle);
    disp(strText);
else;
    bIdentical = false;
    
    strText = sprintf('* Compare : %s -> Different Data !!', strTitle);
    disp(strText);
    strText = sprintf('> First Index : %d', dIndex(1));
    disp(strText);
    dDiffData = [dSrc(dIndex)' dDst(dIndex)'];
    if(length(dIndex) > 10);
        dDiffData(1:10, :)
    else;
        dDiffData(1:end, :)
    end;
end;
