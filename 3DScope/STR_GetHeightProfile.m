function [dProfile] = STR_GetHeightProfile(dData, nStartPosX, nStartPosY, nEndPosX, nEndPosY, bShowGraph)
[nSizeY nSizeX] = size(dData);
nHeight = max(dData(:));

nLength = abs(nEndPosX - nStartPosX) + 1;
dProfile = dData(nStartPosY, nStartPosX:nEndPosX);
dSTD = std(dData(:, nStartPosX:nEndPosX));


%% Eliminate noisy part from both side
dSTD = dSTD./max(dSTD);
nThreshold = 0.2;

nStartPos = 1;
nEndPos = nLength;

for i = 1:nLength;
    if(dSTD(i) < nThreshold);
        break;
    end;
    
    nStartPos = i;
end;

for i = nLength:-1:1;
    if(dSTD(i) < nThreshold);
        break;
    end;
    
    nEndPos = i;
end;

dProfile = dProfile(nStartPos:nEndPos);
nLength = length(dProfile);

if bShowGraph; 
    figure(121);
    clf;
    set(gcf,'position',[300 300 (300+800) (300+100)]);
    
    subplot(1,2,1);
    hold on;
    mesh(dData);
    axis([0 nSizeX 0 nSizeY 0 nHeight]);
    view([0 -90]); 

    plot(nStartPosX, nStartPosY, 'rx');
    szText = sprintf('Start Point (%d, %d)', nStartPosX, nStartPosY); 
    text(nStartPosX, nStartPosY + 50, szText, 'Horizontalalignment','center');
    
    plot(nEndPosX, nEndPosY, 'bx');
    szText = sprintf('End Point (%d, %d)', nEndPosX, nEndPosY); 
    text(nEndPosX, nEndPosY - 50, szText, 'Horizontalalignment','center');

    szText = sprintf('Length (%d)', nLength); 
    text((nEndPosX+nStartPosX)/2, (nEndPosY+nStartPosY)/2, szText, 'Horizontalalignment','center');
    
    plot([nStartPosX nEndPosX], [nStartPosY nEndPosY], 'k');
    hold off;
    
    subplot(1,2,2);
    hold on;
    plot(dProfile, 'b');
    nHeightMargin = (max(dProfile) - min(dProfile)) * 0.1;
    axis([0 nLength (min(dProfile)-nHeightMargin) (max(dProfile)+nHeightMargin)]);
    hold off;
    drawnow;
end;
