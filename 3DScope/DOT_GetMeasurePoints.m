function [bFound dMeasurePoints] = DOT_GetMeasurePoints(dData, dRefData, dHorizMinPoints, dVertMinPoints, bMeasureAll, szAngle, bShowGraph)
[nSizeY nSizeX] = size(dData);
nHeight = max(dData(:));

dMeasurePoints = [];
bFound = false;

if(bMeasureAll);
    nIndex = 1;
    for(nIndexY=1:length(dVertMinPoints));
        for(nIndexX=1:length(dHorizMinPoints));
            nStartPosY = dVertMinPoints(nIndexY);
            nStartPosX = dHorizMinPoints(nIndexX);
            
            if(dRefData(nStartPosY, nStartPosX) == 1);
                continue;
            end;

            szDir = 'h';
            [bFound nStartPosX nStartPosY nEndPosX nEndPosY] = DOT_SelectEndCenterPoint(dRefData, dHorizMinPoints, dVertMinPoints, nStartPosX, nStartPosY, bMeasureAll, szAngle, szDir);
            if(bFound);
                dMeasurePoints(nIndex).nStartPosX = nStartPosX;
                dMeasurePoints(nIndex).nStartPosY = nStartPosY;
                dMeasurePoints(nIndex).nEndPosX = nEndPosX;
                dMeasurePoints(nIndex).nEndPosY = nEndPosY;
                nIndex = nIndex + 1;
            end;

            nStartPosY = dVertMinPoints(nIndexY);
            nStartPosX = dHorizMinPoints(nIndexX);
            
            szDir = 'v';
            [bFound nStartPosX nStartPosY nEndPosX nEndPosY] = DOT_SelectEndCenterPoint(dRefData, dHorizMinPoints, dVertMinPoints, nStartPosX, nStartPosY, bMeasureAll, szAngle, szDir);
            if(bFound);
                dMeasurePoints(nIndex).nStartPosX = nStartPosX;
                dMeasurePoints(nIndex).nStartPosY = nStartPosY;
                dMeasurePoints(nIndex).nEndPosX = nEndPosX;
                dMeasurePoints(nIndex).nEndPosY = nEndPosY;
                nIndex = nIndex + 1;
            end;
        end;
    end;
else;
    % Select Start Points
    dDiffCenter = abs(dHorizMinPoints - nSizeX/2);
    nIndex = find(dDiffCenter == min(dDiffCenter));
    nStartPosX = dHorizMinPoints(nIndex);

    dDiffCenter = abs(dVertMinPoints - nSizeY/2);
    nIndex = find(dDiffCenter == min(dDiffCenter));
    nStartPosY = dVertMinPoints(nIndex);

    if(dRefData(nStartPosY, nStartPosX) == 1);
        % Check right and left side
        bFound = false;
        if(nIndex < length(dHorizMinPoints));
            nStartPosX = dHorizMinPoints(nIndex + 1);
            if(dRefData(nStartPosY, nStartPosX) ~= 1);
                bFound = true;
            end;
        end;
            
        if(bFound == false && nIndex > 1);
            nStartPosX = dHorizMinPoints(nIndex - 1);
            if(dRefData(nStartPosY, nStartPosX) ~= 1);
                bFound = true;
            end;
        end;
            
        if(bFound == false);
            return;
        end;
    end;
    
    nStartPosOrigX = nStartPosX;
    nStartPosOrigY = nStartPosY;
    
    nIndex = 1;

    % Select End Points
    szDir = 'h';
    [bFound nStartPosX nStartPosY nEndPosX nEndPosY] = DOT_SelectEndCenterPoint(dRefData, dHorizMinPoints, dVertMinPoints, nStartPosX, nStartPosY, bMeasureAll, szAngle, szDir);
    if(bFound);
        dMeasurePoints(nIndex).nStartPosX = nStartPosX;
        dMeasurePoints(nIndex).nStartPosY = nStartPosY;
        dMeasurePoints(nIndex).nEndPosX = nEndPosX;
        dMeasurePoints(nIndex).nEndPosY = nEndPosY;
        nIndex = nIndex + 1;
    end;
    
    nStartPosX = nStartPosOrigX;
    nStartPosY = nStartPosOrigY;
    
    szDir = 'v';
    [bFound nStartPosX nStartPosY nEndPosX nEndPosY] = DOT_SelectEndCenterPoint(dRefData, dHorizMinPoints, dVertMinPoints, nStartPosX, nStartPosY, bMeasureAll, szAngle, szDir);
    if(bFound);
        dMeasurePoints(nIndex).nStartPosX = nStartPosX;
        dMeasurePoints(nIndex).nStartPosY = nStartPosY;
        dMeasurePoints(nIndex).nEndPosX = nEndPosX;
        dMeasurePoints(nIndex).nEndPosY = nEndPosY;
        nIndex = nIndex + 1;
    end;
end;

if(length(dMeasurePoints) > 0);
    bFound = true;
end;

%% Show Graph
if bShowGraph; 
    figure(101);
    subplot(2,2,1);
    hold on;
    mesh(dData);
    axis([0 nSizeX 0 nSizeY 0 nHeight]);
    view([0 -90]);    
    for(i=1:length(dMeasurePoints));
        nStartPosX = dMeasurePoints(i).nStartPosX;
        nStartPosY = dMeasurePoints(i).nStartPosY;
        nEndPosX = dMeasurePoints(i).nEndPosX;
        nEndPosY = dMeasurePoints(i).nEndPosY;
        
        plot([nStartPosX nEndPosX], [nStartPosY nEndPosY], 'k');
        plot(nStartPosX, nStartPosY, 'rx');
        plot(nEndPosX, nEndPosY, 'gx');
    end;
    hold off;
    
    drawnow;
end;

