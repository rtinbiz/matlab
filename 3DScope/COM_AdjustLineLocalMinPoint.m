function dCenterPoints = COM_AdjustLineLocalMinPoint(dCenterPoints)
nLength = length(dCenterPoints);
if(nLength < 4);
    return;
end;

nMeanDiff = 0;
for(i=3:nLength-1);
    nMeanDiff = nMeanDiff + (dCenterPoints(i) - dCenterPoints(i-1));
end;
nMeanDiff = floor(nMeanDiff / (nLength - 3))+1;

if((dCenterPoints(2) - dCenterPoints(1))>nMeanDiff);
    dCenterPoints(1) = dCenterPoints(2) - nMeanDiff;
end;

if((dCenterPoints(nLength) - dCenterPoints(nLength-1))>nMeanDiff);
    dCenterPoints(nLength) = dCenterPoints(nLength-1) + nMeanDiff;
end;




