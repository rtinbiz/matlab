function [bFound dHorizCenterPoints dVertCenterPoints dHorizMeanLine dVertMeanLine] = DOT_GetMinCenterPoints(dData, dRefData, bShowGraph)
[nSizeY nSizeX] = size(dData);
nHeight = max(dData(:));
nRefDataHeight = max(dRefData(:));

%% Filter for Line Smoothing
nFilterSize=25;
dFilter=(1/nFilterSize)*ones(1, nFilterSize);

%% Get vertical line data and x position
dHorizMeanLine = mean(dRefData);
dHorizMeanLine=imfilter(dHorizMeanLine,dFilter,'replicate','conv');

nCount = length(find(dRefData == 1));
nLocalMinThreshold = nCount / (nSizeY * nSizeX);
[bFound, dHorizCenterPoints] = COM_FindLineLocalMinPoint(dHorizMeanLine, nLocalMinThreshold);
if(bFound == false);
    bFound = false;
    return;
end;
dHorizCenterPoints = COM_AdjustLineLocalMinPoint(dHorizCenterPoints);

[bFound, dVertCenterPoints1, dVertMeanLine1] = DOT_GetVertMinCenterPoints(dRefData, dHorizMeanLine, nLocalMinThreshold, dFilter, dHorizCenterPoints(1));
if(bFound == false);
    return;
end;

dVertCenterPoints = dVertCenterPoints1;
dVertMeanLine = dVertMeanLine1;

if(length(dHorizCenterPoints) >= 2); 
    [bFound2, dVertCenterPoints2, dVertMeanLine2] = DOT_GetVertMinCenterPoints(dRefData, dHorizMeanLine, nLocalMinThreshold, dFilter, dHorizCenterPoints(2));
    if(bFound2);
		nMinDist = nSizeY/4;
		if(length(dVertCenterPoints)>=2);
			nMinDist = (dVertCenterPoints(2) - dVertCenterPoints(1))/4;
        end;
        
        for(i=1:length(dVertCenterPoints2));
            dTempVertCenterPoints = dVertCenterPoints(find(dVertCenterPoints > (dVertCenterPoints2(i) - nMinDist)));
            dTempVertCenterPoints = dTempVertCenterPoints(find(dTempVertCenterPoints < (dVertCenterPoints2(i) + nMinDist)));
            
            if(isempty(dTempVertCenterPoints));
                dVertCenterPoints = [dVertCenterPoints dVertCenterPoints2(i)];
            end;
        end;
        dVertCenterPoints = sort(dVertCenterPoints);
        dVertMeanLine = (dVertMeanLine + dVertMeanLine2)/2;
    end;    
end;

dVertCenterPoints = COM_AdjustLineLocalMinPoint(dVertCenterPoints);


%% Show Graph
if bShowGraph; 
    figure(101);
    clf;
    subplot(2,2,1);
    mesh(dData);
    axis([0 nSizeX 0 nSizeY 0 nHeight]);
    view([0 -90]);

    if(~isempty(dHorizCenterPoints));
        subplot(2,2,3);
        hold on;
        plot(dHorizMeanLine);
        plot(dHorizCenterPoints, dHorizMeanLine(dHorizCenterPoints), 'rx')    
        axis([0 nSizeX 0 nRefDataHeight]);
        hold off;
    end;
    
    if(~isempty(dVertCenterPoints1));
        subplot(2,2,2);
        hold on;
        plot(dVertMeanLine1,'r');
        plot(dVertCenterPoints, dVertMeanLine1(dVertCenterPoints), 'rx')    
        axis([0 nSizeY 0 nRefDataHeight]);
        view([-90 -90]); 
        hold off;
    end;

    if(~isempty(dVertCenterPoints2));
        subplot(2,2,2);
        hold on;
        plot(dVertMeanLine2, 'b');
        axis([0 nSizeY 0 nRefDataHeight]);
        view([-90 -90]); 
        hold off;
    end;    

    
    subplot(2,2,4);
    mesh(dData);
    axis([0 nSizeX 0 nSizeY 0 nHeight]);
    view([0 -90]);
    
    hold on;
    [dX, dY] = meshgrid(dHorizCenterPoints, dVertCenterPoints);
    plot(dX, dY, 'wx');
    hold off;
    
    drawnow;
end;


