function dFeaturePoint = COM_SelectFeaturePoint(dHeightProfile, dAngleProfile, dAnglePoint, umPerPixel, umPerHeight, bRightPart)
nLength = length(dHeightProfile);
nLengthAnglePoint = length(dAnglePoint);

if(bRightPart);
    dHeightProfile = dHeightProfile(nLength:-1:1);
    dAngleProfile = dAngleProfile(nLength:-1:1);
    dAnglePoint = dAnglePoint(nLengthAnglePoint:-1:1);
    dAnglePoint = nLength - dAnglePoint + 1;
end;

% Finding High position
nMax =  dHeightProfile(dAnglePoint(1));
nHighThreshold = nMax * (1 - 0.07);
nAngleThreshold = 170;
nMinAngle = 180;
for(i=1:nLengthAnglePoint);
    nPos = dAnglePoint(i);
    if(dHeightProfile(nPos) < nHighThreshold);
        break;
    end;
    
    if(dAngleProfile(nPos) < nAngleThreshold && dAngleProfile(nPos) < nMinAngle);
        nHighPos = nPos;
        nMinAngle = dAngleProfile(nPos);
    end;
end; 

nHighPosOppsit = dAnglePoint(end);
nMinAngle = 180;
for(i=nLengthAnglePoint:-1:1);
    nPos = dAnglePoint(i);
    if(dHeightProfile(nPos) < nHighThreshold);
        break;
    end;
    
    if(dAngleProfile(nPos) < nAngleThreshold  && dAngleProfile(nPos) < nMinAngle);
        nHighPosOppsit = nPos;
    end;
end;

% [nMin nIndex] = min(dAngleProfile(nStartPos:nHalfPos));
% nHighPos = nStartPos+nIndex-1;

% Finding Mid position (Candidate)
% find angle points between HighPos and LowPos
nHighThreshold = nMax * (1 - 0.5);
nMinAngle = 180;
for(i=1:nLengthAnglePoint);
    nPos = dAnglePoint(i);
    if(nPos <= nHighPos);
        continue;
    end;

    nAngle = dAngleProfile(nPos);
    if(nAngle < nMinAngle);
        nMidPos = nPos;
        nMinAngle = nAngle;
    end;
    
    if(dHeightProfile(nPos) < nHighThreshold);
        break;
    end;
end;

% Finding Low position (Candidate)
nHalfPos = floor((nHighPos + nHighPosOppsit)/2);
nCheckSize = floor((nHalfPos- nHighPos)/2);

% Check Left Part
% add min height point of left part into dAnglePoint 
[nMin nPos] = min(dHeightProfile(nHalfPos-nCheckSize:nHalfPos));
nPos = nPos + (nHalfPos-nCheckSize) - 1;
if(isempty(find(dAnglePoint == nPos)));
    dAnglePoint = [dAnglePoint nPos];
    dAnglePoint = sort(dAnglePoint);
end;
nLeftPartMinPos = nPos;

% Check Right Part
% add min height point of right part into dAnglePoint 
[nMin nPos] = min(dHeightProfile(nHalfPos:nHalfPos+nCheckSize));
nPos = nPos + nHalfPos - 1;
if(isempty(find(dAnglePoint == nPos)));
    dAnglePoint = [dAnglePoint nPos];
    dAnglePoint = sort(dAnglePoint);
end;
nRightPartMinPos = nPos;

nBarSize = floor(nLength * 0.05);
nHeightLeft = mean(dHeightProfile(nLeftPartMinPos-nBarSize:nLeftPartMinPos-1));
nHeight = dHeightProfile(nLeftPartMinPos);
nHeightRight = mean(dHeightProfile(nLeftPartMinPos+1:nLeftPartMinPos+nBarSize));

if(nHeightLeft >= nHeight && nHeightRight >= nHeight);
    nLowPos = nLeftPartMinPos;
else;
    nLowPos = nRightPartMinPos;
end;

%Adjust Low position
dAnglePoint2 = dAnglePoint(find(dAnglePoint >= nHalfPos-nCheckSize));
dAnglePoint2 = dAnglePoint2(find(dAnglePoint2 <= nLowPos));

nStartPos = dAnglePoint2(1);
nHalfPos = dAnglePoint2(end);

nMax = max(dHeightProfile(nStartPos:nHalfPos));
nMin = min(dHeightProfile(nStartPos:nHalfPos));
nLowThreshold = nMin + (nMax - nMin) * 0.1;

nThresholdLowPosAngle2 = 165;
for(i=length(dAnglePoint2):-1:1);
    nPos = dAnglePoint2(i);
    if(dHeightProfile(nPos) > nLowThreshold);
        break;
    end;
    
    if(dAngleProfile(nPos) < nThresholdLowPosAngle2);
        nLowPos = nPos;
    end;
end;

% Adjust Mid Position 
dX= dAnglePoint(find(dAnglePoint > nMidPos));
dX= dX(find(dX < nLowPos));

dY = dHeightProfile(dX);
nEstMidY = COM_EstimateMidPosHeightByGammaFit(dX, dY, nLowPos, dHeightProfile(nLowPos), nMidPos, dHeightProfile(nMidPos), bRightPart, true);

if(bRightPart);
    dHeightProfile = dHeightProfile(nLength:-1:1);    
    nHighPos = nLength - nHighPos + 1;
    nMidPos = nLength - nMidPos + 1;
    nLowPos = nLength - nLowPos + 1;
end;

dFeaturePoint.nHighPos = nHighPos;
dFeaturePoint.nHighPosHeight = dHeightProfile(nHighPos);
dFeaturePoint.nMidPos = nMidPos;
dFeaturePoint.nMidPosHeight = nEstMidY;
dFeaturePoint.nLowPos = nLowPos;
dFeaturePoint.nLowPosHeight = dHeightProfile(nLowPos);
