function [dData] = STR_GetRefHeightData(dHeightData)
dSTDLine = std(dHeightData);
%% Filter for Line Smoothing
nFilterSize=25;
dFilter=(1/nFilterSize)*ones(1, nFilterSize);
dSTDLine=imfilter(dSTDLine,dFilter,'replicate','conv');

dSTDLine = dSTDLine./max(dSTDLine);
nThreshold = 0.5;

dData = zeros(1, length(dSTDLine));
dIndex = find(dSTDLine < nThreshold);
dData(dIndex) = 1;

% clf;
% hold on;
% plot(dData, 'b');
% plot(dSTDLine, 'r');
% hold off;