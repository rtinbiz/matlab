function [dReference, nDir] = GetReference(dSourceImage, dResultImage, nCenterX, nCenterY, nGapSize)
[nSizeY, nSizeX] = size(dSourceImage);

nHalfGapSize = floor(nGapSize/2);
nStartX = nCenterX - nHalfGapSize;
nEndX = nCenterX + nHalfGapSize;

if(nStartX >= 1 && nEndX <= nSizeX);
    nY = nCenterY;
    while(nY >= 1);
        nY = nY - nGapSize;     % 위쪽 검색
        nStartY = nY - nHalfGapSize;
        nEndY = nY + nHalfGapSize;
        if(nStartY < 1);
            break;
        end;

        dSubResultImage = dResultImage(nStartY:nEndY, nStartX:nEndX);
        dIndex = find(dSubResultImage > 0);
        if(~isempty(dIndex));
            continue;
        end;

        nDir = 1;
        dReference=dSourceImage(nStartY:nEndY, nStartX:nEndX);
        return;
    end;

    nY = nCenterY;
    while(nY <= nSizeY);    
        nY = nY + nGapSize;      % 아래쪽 검색           
        nStartY = nY - nHalfGapSize;
        nEndY = nY + nHalfGapSize;
        if(nEndY > nSizeY);
            break;
        end;

        dSubResultImage = dResultImage(nStartY:nEndY, nStartX:nEndX);
        dIndex = find(dSubResultImage > 0);
        if(~isempty(dIndex));
            continue;
        end;

        nDir = 2;
        dReference=dSourceImage(nStartY:nEndY, nStartX:nEndX);
        return;
    end;
end;

nStartY = nCenterY - nHalfGapSize;
nEndY = nCenterY + nHalfGapSize;

if(nStartY >= 1 && nEndY <= nSizeY);
    nX = nCenterX;
    while(nX >= 1);
        nX = nX - nGapSize;     % 왼쪽 검색
        nStartX = nX - nHalfGapSize;
        nEndX = nX + nHalfGapSize;
        if(nStartX < 1);
            break;
        end;

        dSubResultImage = dResultImage(nStartY:nEndY, nStartX:nEndX);
        dIndex = find(dSubResultImage > 0);
        if(~isempty(dIndex));
            continue;
        end;

        nDir = 3;
        dReference=dSourceImage(nStartY:nEndY, nStartX:nEndX);
        return;
    end;

    nX = nCenterX;
    while(nX <= nSizeX);     % 오른쪽 검색 
        nX = nX + nGapSize;     
        nStartX = nX - nHalfGapSize;
        nEndX = nX + nHalfGapSize;
        if(nEndX > nSizeX);
            break;
        end;

        dSubResultImage = dResultImage(nStartY:nEndY, nStartX:nEndX);
        dIndex = find(dSubResultImage > 0);
        if(~isempty(dIndex));
            continue;
        end;

        nDir = 4;
        dReference=dSourceImage(nStartY:nEndY, nStartX:nEndX);
        return;
    end;
end;

nDir = 0;   % Reference 못찾음.
