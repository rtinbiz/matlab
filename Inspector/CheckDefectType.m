function nDefectType = CheckDefectType(dSourceImage, dResultImage, dBoundingBox, nGapSize)
global NORMAL_DEFECT;
global STICK_UPPER_DEFECT;
global GLASS_UPPER_DEFECT;
global GLASS_LOWER_DEFECT;
global ETCHING_OVER_DEFECT;
global ETCHING_UNDER_DEFECT;
nDefectType = NORMAL_DEFECT;

[nSizeY, nSizeX] = size(dSourceImage);

% X,Y 좌표는 0 based 이기 때문에, maxtrix index 로 바꿔주기 위해 1 씩을 더한다.
nStartX = dBoundingBox(1) + 1;
nStartY = dBoundingBox(2) + 1;
nEndX = dBoundingBox(1) + dBoundingBox(3);
nEndY = dBoundingBox(2) + dBoundingBox(4);

nCenterX = floor((nStartX + nEndX)/2);
nCenterY = floor((nStartY + nEndY)/2);

[dReference, nDir] = GetReference(dSourceImage, dResultImage, nCenterX, nCenterY, nGapSize);
if(nDir == 0);
    bResult = false;
    return;
end;

nValueThreshold = graythresh(dReference) * 255;
dIndex = find(dReference>nValueThreshold);

nHoleValue = mean(dReference(dIndex));
nHoleStd = std(double(dReference(dIndex)));

dIndex = find(dReference<=nValueThreshold);
nSurfaceValue = mean(dReference(dIndex));
nSurfaceStd = std(double(dReference(dIndex)));

% figure(10); 
% imshow(dReference);
% Check Etching Defect

nTotalCount = 0;
nHoleValueCount = 0;
nSlopeValueCount = 0;
nSurfaceValueCount = 0;
nHolePosCount = 0;
nSlopePosCount = 0;
nSurfacePosCount = 0;
nThresholdHoleStd = 1.2;
nThresholdSurfaceStd = 1;
for nY=nStartY:nEndY;
    for nX=nStartX:nEndX; 
        if(dResultImage(nY, nX) == 0);
            continue;
        end;
        
        switch(nDir);
            case 1
                nRefX = nX;
                nRefY = nY - nGapSize;
            case 2
                nRefX = nX;
                nRefY = nY + nGapSize;
            case 3
                nRefX = nX - nGapSize;
                nRefY = nY;
            case 4
                nRefX = nX + nGapSize;
                nRefY = nY;
        end;
        
        % 위치 판별 : Reference 위치로 체크, 
        nValue = double(dSourceImage(nRefY, nRefX));
        if(nValue > nHoleValue - nThresholdHoleStd * nHoleStd);
            nHolePosCount = nHolePosCount + 1;
        elseif(nValue < nSurfaceValue + nThresholdSurfaceStd * nSurfaceStd);
            nSurfacePosCount = nSurfacePosCount + 1;
        else;
            nSlopePosCount = nSlopePosCount + 1;
        end;

        % 불량 영역의 밝기 체크. 밝기에 Hole 또는 Surface 인지 체크한다.
        nValue = double(dSourceImage(nY, nX));
        if(abs(nValue - nHoleValue) < nThresholdHoleStd * nHoleStd);
            nHoleValueCount = nHoleValueCount + 1;
        elseif(abs(nValue - nSurfaceValue) < nThresholdSurfaceStd * nSurfaceStd);
            nSurfaceValueCount = nSurfaceValueCount + 1;
        end;
        
        nTotalCount = nTotalCount + 1;        
    end;
end;
    
bHolePos = false;
bSlopePos = false;
bSurfacePos = false;
bHoleValue = false;
bSurfaceValue = false;

nHolePosRate = nHolePosCount/nTotalCount;
nSlopePosRate = nSlopePosCount/nTotalCount;
nSurfacePosRate = nSurfacePosCount/nTotalCount;

nThresholdRate = 0.9;
if(nHolePosRate > nThresholdRate);
    bHolePos = true;
end;
if(nSlopePosRate > nThresholdRate);
    bSlopePos = true;
end;
if(nSurfacePosRate > nThresholdRate);
    bSurfacePos = true;
end;

nHoleValueRate = nHoleValueCount/nTotalCount;
nSurfaceValueRate = nSurfaceValueCount/nTotalCount;

if(nHoleValueRate > nThresholdRate);
    bHoleValue = true;
elseif(nSurfaceValueRate > nThresholdRate);
    bSurfaceValue = true;
end;

% Etching Over : 불량 위치가 hole 이 아니면서, 밝기가 hole 과 같을 경우
if(~bHolePos && bHoleValue);
    nDefectType = ETCHING_OVER_DEFECT;
    return ;
end;

% Etching Under : 불량 위치가 Surface 가 아니면서, 밝기가 surface 와 같을 경우
if(~bSurfacePos && bSurfaceValue);
    nDefectType = ETCHING_UNDER_DEFECT;
    return;
end;

% Stick Upper : 불량 위치가 hole 일 경우
if(bHolePos);
    nDefectType = STICK_UPPER_DEFECT;
    return ;
end;

% Glass : 불량의 위치가 어느쪽에도 포함되지 않으면 걸쳐 있다는 것을 의미함.
if(~bSurfacePos && ~bSlopePos && ~bHolePos);
    nDefectType = GLASS_UPPER_DEFECT;
    return ;
end;    
    
    



