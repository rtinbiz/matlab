clear all;
close all;
format shortg;

global NORMAL_DEFECT;
global STICK_UPPER_DEFECT;
global GLASS_UPPER_DEFECT;
global GLASS_LOWER_DEFECT;
global ETCHING_OVER_DEFECT;
global ETCHING_UNDER_DEFECT;
NORMAL_DEFECT = 1;
STICK_UPPER_DEFECT = 10;		% 스틱 상면
GLASS_UPPER_DEFECT = 20;		% 글라스 상면
GLASS_LOWER_DEFECT = 21;		% 글라스 하면
ETCHING_OVER_DEFECT = 30;	% 과에칭
ETCHING_UNDER_DEFECT = 31; % 미에칭

dGrabImagePath = 'Z:\\DefectData\\Src.BMP';
dGrabImage = imread(dGrabImagePath);
% figure(1); imshow(dGrabImage);

% % nX = 1583;
% % nY = 1143;
% nX = 1584;
% nY = 1143;
% nGap = 440;
% 
% dResult = zeros(size(dGrabImage));
% 
% nX = 2000:5000;
% nY = 2000:5000;
% 
% dResult(nY,nX) = abs(dGrabImage(nY, nX) - dGrabImage(nY+nGap, nX+nGap));
% 
% %dIndex = find(dResult ~= 0)
% 
% figure(2); imshow(dResult);


dResultImagePath = 'Z:\\DefectData\\Rst.BMP';
dResultImage = imread(dResultImagePath);

dBWImage = dResultImage;
dIndex = find(dBWImage >= 1);
dBWImage(dIndex) = 1;

dLabel = bwlabel(dBWImage);
dStat = regionprops(dLabel, 'all');
%dIndex = find([dStat.MinorAxisLength]./[dStat.MajorAxisLength] <= nLengthRate);
%dResultImage = ismember(dLabel, dIndex);

dCenter = [dStat.Centroid];
dX = dCenter(1:2:end-1);
dY = dCenter(2:2:end);

figure(2); 
clf;
hold on;
image(dBWImage*255);
plot(dX, dY, 'rx')
view(0, -90);
set(gcf,'unit','normalized','outerposition',[0,0,1,1]);    
hold off;

nMinSize = 5;
nMaxSize = 50;
nGapSize = 440;

bCheckEtchingOver = false;
bCheckEtchingUnder = true;

figure(3); 
hold on;
nIndex = 0;
nCheckIndex = [];
for i=1:length(dStat);
    dBoundingBox = floor(dStat(i).BoundingBox);
    
    if(dBoundingBox(3) < nMinSize && dBoundingBox(4) < nMinSize);
        continue;
    end;
    if(dBoundingBox(3) > nMaxSize || dBoundingBox(4) > nMaxSize);
        continue;
    end;
    if(dBoundingBox(3) == 1 || dBoundingBox(4) == 1);
        continue;
    end;
    
    fprintf('(%d) : [%d %d %d %d]\n', nIndex, dBoundingBox(1), dBoundingBox(2), ...
        dBoundingBox(1)+dBoundingBox(3) - 1, dBoundingBox(2)+dBoundingBox(4) - 1);
    
    if(~isempty(nCheckIndex) && nCheckIndex ~= nIndex);
        nIndex = nIndex + 1;
        continue;
    end;
    nIndex = nIndex + 1;
    
    nSizeX = dBoundingBox(3);
    nSizeY = dBoundingBox(4);

    if(nSizeX*nSizeY < 10000);
        nExtSizeX = nSizeX * 7;
        nExtSizeY = nSizeY * 7;
    else;
        nExtSizeX = nSizeX;
        nExtSizeY = nSizeY;
    end;
    
    nStartX = dBoundingBox(1) - nExtSizeX;
    nStartY = dBoundingBox(2) - nExtSizeY;
    nEndX = dBoundingBox(1) + nSizeX + nExtSizeX;
    nEndY = dBoundingBox(2) + nSizeY + nExtSizeY;
    
    dDefectImage = dGrabImage(nStartY:nEndY, nStartX:nEndX);
    dDefectResultImage = dBWImage(nStartY:nEndY, nStartX:nEndX);
    
    subplot(1,2,1); imshow(dDefectImage);
    subplot(1,2,2); imshow(dDefectResultImage*255);
    set(gcf,'unit','normalized','outerposition',[0,0,1,1]);    
    drawnow;
   
    nDefectType = CheckDefectType(dGrabImage, dBWImage, dBoundingBox, nGapSize);
    switch (nDefectType)
        case NORMAL_DEFECT
            fprintf('  --> Normal Defect (1)\n');
        case STICK_UPPER_DEFECT
            fprintf('  --> Stick Upper Defect (10)\n');
        case GLASS_UPPER_DEFECT
            fprintf('  --> Glass Upper Defect (20)\n');
        case GLASS_LOWER_DEFECT
            fprintf('  --> Glass Lower Defect (21)\n');
        case ETCHING_OVER_DEFECT
            fprintf('  --> Etching Over Defect (30)\n');
        case ETCHING_UNDER_DEFECT
            fprintf('  --> Etching Under Defect (31)\n');
    end;
    pause;
end;
